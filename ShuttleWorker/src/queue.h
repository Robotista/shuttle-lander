#ifndef QUEUE_H
#define QUEUE_H

#include "shuttle.h"

typedef struct _queue {
	stroke* array;
	int front, rear, size;
	unsigned capacity;
} queue;

queue* create_queue(unsigned capacity);
int is_empty(queue* queue);
void enqueue(queue* queue, stroke item);
stroke dequeue(queue* queue);
void clear_queue(queue* queue);

#endif // QUEUE_H
