
// Copyright 2013 Eric Messick (FixedImagePhoto.com/Contact)
// Modified for Shuttle Lander by Robotista (cybertic.cz/about)

#ifndef SHUTTLE_H
#define SHUTTLE_H

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <dirent.h> 

#include <linux/input.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <regex.h>

#include <X11/Xlib.h>
#include <stdbool.h>

// protocol for events from the shuttlepro HUD device
//
// ev.type values:
#define EVENT_TYPE_DONE 0
#define EVENT_TYPE_KEY 1
#define EVENT_TYPE_JOGSHUTTLE 2
#define EVENT_TYPE_ACTIVE_KEY 4

// ev.code when ev.type == KEY
#define EVENT_CODE_KEY1 256
// KEY2 257, etc...

// ev.value when ev.type == KEY
// 1 -> PRESS; 0 -> RELEASE

// ev.code when ev.type == JOGSHUTTLE
#define EVENT_CODE_JOG 7
#define EVENT_CODE_SHUTTLE 8
#define EVENT_CODE_ACTIVE_KEY 11


// ev.value when ev.code == JOG
// 8 bit value changing by one for each jog step

// ev.value when ev.code == SHUTTLE
// -7 .. 7 encoding shuttle position

// Modifiers
#define PRESS 1
#define RELEASE 2
#define PRESS_RELEASE 3
#define HOLD 4
#define REPEAT 5

#define NUM_KEYS 15
#define NUM_SHUTTLES 15
#define NUM_JOGS 2

typedef enum _stroke_type {
	STROKE_PRESS,
	STROKE_RELEASE,
	STROKE_REPEAT_START,
	STROKE_REPEAT_STOP
} stroke_type; 

typedef struct _stroke {
	struct _stroke *next;
	int				key_code;
	stroke_type 	type; // what to do with this key - press, release, repeat, stop repeat
	int 			repeat_time; // time for repeating in ms
} stroke;

typedef struct _modifier_assignation {
	int				key_code;
	int 			modifier; 
} modifier_assignation;

#define KJS_KEY_DOWN 1
#define KJS_KEY_UP 2
#define KJS_SHUTTLE 3
#define KJS_JOG 4

typedef struct _translation {
	struct _translation *next;
	char *name;
	bool is_default;
	regex_t regex;
	stroke *key_down[NUM_KEYS];
	stroke *key_up[NUM_KEYS];
	stroke *shuttle[NUM_SHUTTLES];
	stroke *jog[NUM_JOGS];
} translation;

extern void 		read_config_file();
extern translation*	get_translation(char *win_title);

#endif // SHUTTLE_H
