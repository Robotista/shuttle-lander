#include <linux/uinput.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include "shuttle.h"
#include "queue.h"

int				keyboardFileHandler = 0;
pthread_t 		thread;
pthread_cond_t 	cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
queue* 			stroke_queue;
_Atomic bool	thread_terminated = false;

void populate_codes(int filehandle)
{
	for (unsigned int i = KEY_ESC; i <= KEY_SCALE; i++)	{
		ioctl(filehandle, UI_SET_KEYBIT, i);
	}
	
	ioctl(filehandle, UI_SET_RELBIT, REL_WHEEL);
	ioctl(filehandle, UI_SET_KEYBIT, BTN_LEFT);
	ioctl(filehandle, UI_SET_KEYBIT, BTN_RIGHT);
	ioctl(filehandle, UI_SET_KEYBIT, BTN_MIDDLE);
	ioctl(filehandle, UI_SET_KEYBIT, KEY_VOLUMEUP);
	ioctl(filehandle, UI_SET_KEYBIT, KEY_VOLUMEDOWN);
	ioctl(filehandle, UI_SET_KEYBIT, KEY_BRIGHTNESSUP);
	ioctl(filehandle, UI_SET_KEYBIT, KEY_BRIGHTNESSDOWN);
	ioctl(filehandle, UI_SET_KEYBIT, KEY_SLEEP);
}

int init_device()
{
	int device = -1;	
	device = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
	if (device < 0)	{
		printf("Unable to open UINPUT device.\n");
		return device;
	}
	
	ioctl(device, UI_SET_EVBIT, EV_KEY);
	ioctl(device, UI_SET_EVBIT, EV_SYN);
	ioctl(device, UI_SET_EVBIT, EV_REL);

	populate_codes(device);

	struct uinput_setup usetup;
	memset(&usetup, 0, sizeof(usetup));
	usetup.id.bustype = BUS_USB;
	usetup.id.vendor = 0x0;
	usetup.id.product = 0x0;
	usetup.id.version = 1;
	strcpy(usetup.name, "Shuttle Emulated Keyboard");
	ioctl(device, UI_DEV_SETUP, &usetup);
	
	if (ioctl(device, UI_DEV_CREATE, NULL) < 0)	{
		printf("Unable to create UINPUT device.\n");
	}
	
	return device;
}

void close_device(int filehandle)
{
	ioctl(filehandle, UI_DEV_DESTROY);
	close(filehandle);
}

void uinput_cleanup()
{
	thread_terminated = true;

	pthread_mutex_lock( &mutex );
	pthread_cond_signal(&cond);
	pthread_mutex_unlock( &mutex);
	pthread_cond_destroy(&cond);
	pthread_join(thread, NULL);
	
	clear_queue(stroke_queue);
}

void simulate_keyboard_event(int code, int pressed)
{
	struct input_event ev;
	struct input_event ev2;	
	int isRelEvent = code > 1000;
	
	// REL event, key UP is not relevant and can be ignored
	if (isRelEvent && pressed == 0)
		return;
		
	// Decode the REL events (offsetted by 1000 or 2000, according to their direction)
	if (code / 1000 == 1)
		pressed = -1;
	
	if (code / 1000 == 2)
		pressed = 1;
	
	code = code % 1000;
	
	memset(&ev, 0, sizeof(struct input_event));
	gettimeofday(&ev.time, NULL);
	ev.type = isRelEvent ? EV_REL : EV_KEY;
	ev.code = code;
	ev.value = pressed;	
	if (write(keyboardFileHandler, &ev, sizeof(struct input_event)) != sizeof(struct input_event))
		return;
	
	memset(&ev2, 0, sizeof(struct input_event));
	gettimeofday(&ev2.time, NULL);
	ev2.type = EV_SYN;
	ev2.code = SYN_REPORT;
	ev2.value = 0;
	if (write(keyboardFileHandler, &ev2, sizeof(struct input_event)) != sizeof(struct input_event))
		return;
	
}

void uinput_postStroke(stroke new_stroke)
{
	pthread_mutex_lock( &mutex );
	enqueue(stroke_queue, new_stroke);
	pthread_cond_signal(&cond);
	pthread_mutex_unlock( &mutex);
}

void* thread_run()
{
	keyboardFileHandler = init_device();		
	
	stroke proccessed_stroke = {NULL, 0, 0, 0};
	
	while (!thread_terminated)
	{		
		pthread_mutex_lock(&mutex);
		
		if (is_empty(stroke_queue) && proccessed_stroke.type != STROKE_REPEAT_START)
			pthread_cond_wait(&cond, &mutex); // Go to sleep if htere are no keystrokes to process
			
			if (!is_empty(stroke_queue))
				proccessed_stroke = dequeue(stroke_queue);
			
			pthread_mutex_unlock(&mutex);
		
		// If we have a new keystroke, lets proccess it...
		if (proccessed_stroke.type == STROKE_REPEAT_START) {	
			simulate_keyboard_event(proccessed_stroke.key_code, 1);
			simulate_keyboard_event(proccessed_stroke.key_code, 0);
						
			struct timespec ts;
			ts.tv_sec = proccessed_stroke.repeat_time / 1000;
			ts.tv_nsec = (proccessed_stroke.repeat_time % 1000) * 1000000;
			nanosleep(&ts, NULL);
		}		
		
		if (proccessed_stroke.type == STROKE_PRESS)	{
			simulate_keyboard_event(proccessed_stroke.key_code, 1);
		}
		
		if (proccessed_stroke.type == STROKE_RELEASE) {
			simulate_keyboard_event(proccessed_stroke.key_code, 0);
		}
	}
	
	// uinput_cleanup
	if (keyboardFileHandler > 0) {
		close_device(keyboardFileHandler);
		keyboardFileHandler = 0;
	}
	
	return NULL;
}

void uinput_init()
{
	stroke_queue = create_queue(100);	
	thread_terminated = false;
	pthread_create(&thread, NULL, thread_run, NULL);
}
