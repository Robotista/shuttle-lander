#include "queue.h"
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include "shuttle.h"

queue* create_queue(unsigned capacity)
{
	queue* stroke_queue = (queue*)malloc(sizeof(queue));
	stroke_queue->capacity = capacity;
	stroke_queue->front = stroke_queue->size = 0;
	stroke_queue->rear = capacity - 1;
	stroke_queue->array = (stroke*)malloc(stroke_queue->capacity * sizeof(stroke));
	return stroke_queue;
}

int is_empty(queue* stroke_queue)
{
	return (stroke_queue->size == 0);
}

void enqueue(queue* stroke_queue, stroke item)
{
	if (stroke_queue->size == stroke_queue->capacity)
		return;
	
	stroke_queue->rear = (stroke_queue->rear + 1)	% stroke_queue->capacity;
	stroke_queue->array[stroke_queue->rear] = item;
	stroke_queue->size = stroke_queue->size + 1;
}

stroke dequeue(queue* stroke_queue)
{	
	if (is_empty(stroke_queue)) {
		stroke dummy = {NULL, 0, 0, 0};
		return dummy;
	}
	
	stroke item = stroke_queue->array[stroke_queue->front];
	stroke_queue->front = (stroke_queue->front + 1) % stroke_queue->capacity;
	stroke_queue->size = stroke_queue->size - 1;
	return item;
}

void clear_queue(queue* stroke_queue)
{
	free(stroke_queue->array);
	free(stroke_queue);
}
