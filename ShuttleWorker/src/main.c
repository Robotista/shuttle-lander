/*
 * Contour ShuttlePro v2 interface
 * Copyright 2013 Eric Messick (FixedImagePhoto.com/Contact)
 * Based on a version (c) 2006 Trammell Hudson <hudson@osresearch.net>
 * which was in turn
 * Based heavily on code by Arendt David <admin@prnet.org>
 * Modified for Shuttle Lander by Robotista (cybertic.cz/about)
 */

#include "shuttle.h"
#include "uinput_handler.h"
#include <signal.h>
#include <stdio.h>
#include <pwd.h>
#include <time.h>

extern int debug_regex;
extern translation *default_translation;

unsigned short	jogvalue = 0xffff;
int 			shuttlevalue = 0xffff;
struct timeval	last_shuttle;
int				need_synthetic_shuttle;
_Atomic bool	terminated = false;
Display*		display;

stroke *
fetch_stroke(translation *tr, int kjs, int index)
{
	if (tr != NULL) {
		switch (kjs) {
			case KJS_SHUTTLE:
				return tr->shuttle[index];
			case KJS_JOG:
				return tr->jog[index];
			case KJS_KEY_UP:
				return tr->key_up[index];
			case KJS_KEY_DOWN:
			default:
				return tr->key_down[index];
		}
	}
	return NULL;
}

void
send_stroke_sequence(translation *tr, int kjs, int index) 
{
	stroke *s;
	
	s = fetch_stroke(tr, kjs, index);
	if (s == NULL)
	{
		s = fetch_stroke(default_translation, kjs, index);
	}
	while (s)
	{
		//printf("key %d press %d time %d\n", s->key_code, s->type, s->repeat_time);
		uinput_postStroke(*s);
		s = s->next;
	}
}

void
key(unsigned short code, unsigned int value, translation *tr)
{
	code -= EVENT_CODE_KEY1;
	
	if (code <= NUM_KEYS) {
		send_stroke_sequence(tr, value ? KJS_KEY_DOWN : KJS_KEY_UP, code);
	}
	else {
		fprintf(stderr, "key(%d, %d) out of range\n", code + EVENT_CODE_KEY1, value);
	}
}


void
shuttle(int value, translation *tr)
{
	//printf("shuttle %d.\n", value);
	
	if (value < -7 || value > 7) {
		fprintf(stderr, "shuttle(%d) out of range\n", value);
	}
	else {
		gettimeofday(&last_shuttle, 0);
		need_synthetic_shuttle = value != 0;
		if( value != shuttlevalue )
		{
			shuttlevalue = value;
			send_stroke_sequence(tr, KJS_SHUTTLE, value+7);
		}
	}
}

// Due to a bug (?) in the way Linux HID handles the ShuttlePro, the
// center position is not reported for the shuttle wheel.  Instead,
// a jog event is generated immediately when it returns.  We check to
// see if the time since the last shuttle was more than a few ms ago
// and generate a shuttle of 0 if so.
//
// Note, this fails if jogvalue happens to be 0, as we don't see that
// event either!
void
jog(unsigned int value, translation *tr) 
{
	//printf("jog %d.\n", value);
	
	int direction;
	struct timeval now;
	struct timeval delta;
	
	// We should generate a synthetic event for the shuttle going
	// to the home position if we have not seen one recently
	if (need_synthetic_shuttle)
	{
		gettimeofday( &now, 0 );
		timersub( &now, &last_shuttle, &delta );
		
		if (delta.tv_sec >= 1 || delta.tv_usec >= 5000) {
			shuttle(0, tr);
			need_synthetic_shuttle = 0;
		}
	}
	
	if (jogvalue != 0xffff)
	{
		value = value & 0xff;
		direction = ((value - jogvalue) & 0x80) ? -1 : 1;
		while (jogvalue != value) {
			// driver fails to send an event when jogvalue == 0
			if (jogvalue != 0) {
				send_stroke_sequence(tr, KJS_JOG, direction > 0 ? 1 : 0);
			}
			jogvalue = (jogvalue + direction) & 0xff;
		}
	}
	jogvalue = value;
}

void
jogshuttle(unsigned short code, unsigned int value, translation *tr)
{
	switch (code)
	{
		case EVENT_CODE_JOG:
			jog(value, tr);
			break;
		case EVENT_CODE_SHUTTLE:
			shuttle(value, tr);
			break;
		case EVENT_CODE_ACTIVE_KEY:
			break;
		default:
			fprintf(stderr, "jogshuttle(%d, %d) invalid code\n", code, value);
			break;
	}
}

char *
get_window_name(Window win)
{
	Atom prop = XInternAtom(display, "WM_NAME", False);
	Atom type;
	int form;
	unsigned long remain, len;
	unsigned char *list;
	
	if (XGetWindowProperty(display, win, prop, 0, 1024, False,
		AnyPropertyType, &type, &form, &len, &remain,
		&list) != Success) {
		fprintf(stderr, "XGetWindowProperty failed for window 0x%x\n", (int)win);
		return NULL;
		}
		
		return (char*)list;
}

char *
walk_window_tree(Window win)
{
	char *window_name;
	Window root = 0;
	Window parent;
	Window *children;
	unsigned int nchildren;
	
	while (win != root)
	{
		window_name = get_window_name(win);
		if (window_name != NULL)
		{
			return window_name;
		}
		if (XQueryTree(display, win, &root, &parent, &children, &nchildren))
		{
			win = parent;
			XFree(children);
		} else
		{
			fprintf(stderr, "XQueryTree failed for window 0x%x\n", (int)win);
			return NULL;
		}
	}
	return NULL;
}

static Window last_focused_window = 0;
static translation *last_window_translation = NULL;

translation* 
get_focused_window_translation()
{
	// TODO: Implement getting of caption of focused window under Wayland - which is quite tricky. Now we are going just with default translation.
	
	if (!display)		
		return default_translation; // Just go with default translation - we are probably under Wayland
	
	Window focus;
	int revert_to;
	char *window_name = NULL;
	char *name;
	
	XGetInputFocus(display, &focus, &revert_to);
	
	if (focus == None)		
		return default_translation;
		
	if (focus != last_focused_window)
	{
		last_focused_window = focus;
		window_name = walk_window_tree(focus);
		printf("window: %s\n", window_name);
		
		if (window_name == NULL) {
			name = "-- Unlabeled Window --";
		} 
		else {
			name = window_name;
		}
		last_window_translation = get_translation(name);
		if (debug_regex) {
			if (last_window_translation != NULL) {
				printf("translation: %s for %s\n", last_window_translation->name, name);
			}
			else {
				printf("no translation found for %s\n", name);
			}
		}
		if (window_name != NULL) {
			XFree(window_name);
		}
	} 
	return last_window_translation;
}

void 
handle_event(struct input_event ev)
{
	translation* tr = get_focused_window_translation();
	
	//printf("event: (%d, %d, 0x%x)\n", ev.type, ev.code, ev.value);
	if (tr != NULL)
	{
		switch (ev.type)
		{
			case EVENT_TYPE_DONE:
			case EVENT_TYPE_ACTIVE_KEY:
				break;
			case EVENT_TYPE_KEY:
				key(ev.code, ev.value, tr);
				break;
			case EVENT_TYPE_JOGSHUTTLE:
				jogshuttle(ev.code, ev.value, tr);
				break;
			default:
				fprintf(stderr, "handle_event() invalid type code\n");
				break;
		}
	}
}

bool 
check_supported(int fd)
{
	int supported_devices[3][2] = {
		{0x0b33, 0x0010}, // Contour Design ShuttlePro v1
		{0x0b33, 0x0030}, // Contour Design ShuttlePro v2
		{0x0b33, 0x0020} // Contour Design ShuttleXpress
	};
	
	short devinfo[4];	
	if (ioctl(fd, EVIOCGID, &devinfo)) {
		perror("evdev ioctl");
		return false;
	}
	
	for (int i = 0; i < 3 ; i++) {
		if (supported_devices[i][0] == devinfo[1] && supported_devices[i][1] == devinfo[2])
		return true;
	}
	
	return false;
}

bool 
is_shuttle_device(const char *devname)
{
	int fd = open(devname, O_RDONLY);
	if (fd < 0) {
		return false;
	} else {
		if (check_supported(fd)) {
			close(fd);
			return true;
		}
		close(fd);
		return false;
	}
}

bool
get_shuttle_device(char *dev_path)
{
	char* dev_dir_str = "/dev/input/by-id";
	DIR* input_dir = opendir(dev_dir_str);
	if (input_dir == NULL) {
		dev_dir_str = "/dev/input";
		input_dir = opendir(dev_dir_str);
	}
	
	struct dirent *dir;
	if (input_dir) {
		while ((dir = readdir(input_dir)) != NULL) {
			char full_path[300];
			snprintf(full_path, sizeof(full_path), "%s/%s", dev_dir_str, dir->d_name);
			
			if (dir->d_name[0] != '.' && is_shuttle_device(full_path))
			{
				printf("Shuttle device detected on %s\n", full_path);
				snprintf(dev_path, sizeof(full_path), "%s", full_path);
				return true;
			}
		}
		closedir(input_dir);
	}
	printf("Shuttle device not detected\n");
	return false;
}

void
signalHandler(int sig)
{ 
	terminated = true;
}


// Is the current user (the one who runs this app) using the Wayland session?
bool runsOnWayland()
{
	uid_t uid = geteuid();
	struct passwd *pw = getpwuid(uid);
	if (!pw)
	{
		printf("Failed to run getpwuid.\n" );
		exit(1);
	}
	
	char loginctl_cmd[256];
	snprintf(loginctl_cmd, sizeof(loginctl_cmd), "loginctl show-session `loginctl|grep %s|awk '{print $1}'` -p Type", pw->pw_name);
		
	FILE *fp;
	fp = popen(loginctl_cmd, "r");
	if (!fp)
	{
		printf("Failed to run loginctl to check the Wayland session.\n" );
		exit(1);
	}
	
	char loginctl_output[512];
	bool result = false;
	
	while (fgets(loginctl_output, sizeof(loginctl_output), fp) != NULL)
	{
		if (strstr(loginctl_output, "wayland"))
			result = true;
	}
	
	pclose(fp);	
	return result;
}

int
main(int argc, char **argv)
{
	struct sigaction action;
	memset(&action, 0, sizeof(action));
	action.sa_handler = signalHandler;
	sigaction(SIGTERM, &action, NULL);
	sigaction(SIGINT, &action, NULL);	

	// Get the (first found) connected Shuttle device
	char shuttle_device_path[257];
	if (argc > 1)
		snprintf(shuttle_device_path, sizeof(shuttle_device_path), "%s", argv[1]);
	else
	{
		bool shuttle_device_found = get_shuttle_device(shuttle_device_path);
		if (!shuttle_device_found)
			return 1;
	}
		
	uinput_init();
	
	struct input_event ev;
	int nread;
	int fd;
   
	if (runsOnWayland())
		printf("Running on Wayland\n");
	else
	{
		printf("Running on X11\n");
		display = XOpenDisplay(0);
	}
	
	// Load the default translation
	get_translation("");
	
	fd = open(shuttle_device_path, O_RDONLY);
	if (fd < 0) 
	{
		perror(shuttle_device_path);
		return 1;
	}
	else 
	{
		// Flag it as exclusive access
		if(ioctl( fd, EVIOCGRAB, 1 ) < 0) {
			perror( "evgrab ioctl" );
		}
		else{
			while (!terminated)
			{
				fd_set read_fds;
				FD_ZERO(&read_fds);
				FD_SET(fd, &read_fds);
				
				// Set timeout to 1.0 seconds
				struct timeval timeout;
				timeout.tv_sec = 1;
				timeout.tv_usec = 0;
				
				if (select(fd + 1, &read_fds, NULL, NULL, &timeout) == 1)
				{
					nread = read(fd, &ev, sizeof(ev));
					if (nread == sizeof(ev))
						handle_event(ev);
					else
					{
						if (nread < 0){
							break;
						} else{
							fprintf(stderr, "short read: %d\n", nread);
							break;
						}
					}
				}
				
				// Try reload the shuttlerc file every 20s (will reload only if the file actually changed)
				static int sec_to_update = 20;
				if (sec_to_update-- == 0)
				{
					sec_to_update = 20;
					read_config_file();
				}
			}
		}
	}
	close(fd);
	uinput_cleanup();
	
	if (display)
		XCloseDisplay(display);
	
	return 0;
}
