#ifndef UINPUTHANDLER_H
#define UINPUTHANDLER_H

void uinput_init();
void uinput_postStroke(stroke new_stroke);
void uinput_cleanup();

#endif // UINPUTHANDLER_H
