# Shuttle Lander

Shuttle Lander is the Linux application allowing you to convert the control inputs from Shuttle jog controllers (generally ShuttlePro and ShuttleXpress) into virtual keyboard keypresses. This allows you to use the Shuttle controller with the applications that are not natively supporting it. Similar function is provided by official Shuttle Drivers for Windows and Mac. Device driver itself is not a subject of this project, Shuttle devices are using the universal "hidraw" driver already integrated in Linux kernel. Project is building heavily on Nanosyzygy's ShuttlePRO project (https://github.com/nanosyzygy/ShuttlePRO), but lots of features have been added. Mainly:

- Wayland support
- GUI for configuration
- Key repeating possibility (ex. to fire the stream of arrow keys during the shuttling)
- Shuttle device autodetection (filling of device name no longer needed)
- Installation package available

Project is now containing two applications. **ShuttleWorker** is the application running in the background when the Shuttle device is connected. This application is reading the inputs from Shuttle device and providing the virtual keyboard/mouse keypresses. This is the app based on Nanosyzygy's ShuttlePRO. Then there is **ShuttleLander** app. ShuttleLander is providing the configuration GUI.

**User Guide here:** https://gitlab.com/Robotista/shuttle-lander/-/wikis/Shuttle-Lander-User-Guide

## How to get the installation package

There is a DEB installation package (for Debian based distos - Ubuntu, Kubuntu, Pop!_OS, Mint, MX Linux, etc... ) and RPM installation package (for Red Hat based distros - CentOS, Fedora, OpenSUSE, etc...) available. It can be downloaded here:
https://cybertic.cz/downloads/

## How to build from source

### Installing the Prerequisites:

#### Debian derived distros (Ubuntu before 23.04):

```sh
sudo apt install git build-essential libwxgtk3.0-gtk3-dev cmake extra-cmake-modules
```

#### Debian derived distros (Ubuntu 23.04 and later):

```sh
sudo apt install git build-essential libwxgtk3.2-dev cmake extra-cmake-modules
```

#### Red Hat derived distros:

```sh
sudo yum install gcc-c++ git wxGTK3-devel cmake extra-cmake-modules
```

#### Arch derived distros:

```sh
sudo pacman -Syu base-devel git wxwidgets-gtk3 cmake extra-cmake-modules
```

### Building:

```sh
git clone https://gitlab.com/Robotista/shuttle-lander
cd shuttle-lander
mkdir build && cd build
cmake ..
cmake --build .
sudo cmake --install .
```
