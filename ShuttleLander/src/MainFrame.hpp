﻿#ifndef MAINFRAME_H
#define MAINFRAME_H

#include "ShuttleLander_GUI.h"
#include "EditAppDialog.hpp"
#include <array>
#include <map>

struct ShuttleStroke
{
	int					keyCode;
	int 				modifierKeyCode;
	int 				repeatTime;
	VirtualKeyBehavior 	virtualKeyBehavior;
};

struct TranslationSection
{
	wxString						name;
	wxString 						regex;
	bool 							isDefault;
	std::map<wxString, ShuttleStroke> keyMap;
};

class MainFrame
	: public MainFrame_design,
	public ShuttleButtonPickerObserver
{

public:
	MainFrame(wxWindow* parent = nullptr);
	
	void editAppDialogCallback(bool newApp, wxString appName, wxString regExp);

private:
	std::string getTokenFlag(const std::string &token);
	std::string getTokenKey(const std::string &token);
	int			strToInt(const std::string &input, int defaultValue);
	void 		loadShuttlerc();
	int 		checkSupported(int fd);
	bool 		isShuttleDevice(std::string devname, int& shuttleDevice);
	bool 		getShuttleDevice(ShuttleDevice& shuttleDevice, wxString& shuttleDeviceName);
	wxString 	getConfigPath();
	void 		refreshButtonLabels();
	
	// Inherited from ShuttleButtonPickerObserver
	void OnShuttleButtonSelected(ControlElement controlElement);
	EditAppDialog*	m_editAppDialog;
	
private:
	std::vector<TranslationSection>	m_translationSections;

protected:
	void OnClose(wxCloseEvent& event);
	void OnButtonApply(wxCommandEvent& event);
	void OnButtonAppEdit(wxCommandEvent& event);
	void OnButtonAppRemove(wxCommandEvent& event);
	void OnButtonRefreshDevices(wxCommandEvent& event);
	void OnComboApplication( wxCommandEvent& event );
	void OnComboKeyBehavor( wxCommandEvent& event );
	void OnComboKey(wxCommandEvent& event);
	void OnComboModifierKey(wxCommandEvent& event);
	void OnSpinRepeatTime( wxSpinEvent& event );
	void OnMenuAbout(wxCommandEvent& event);
	void OnComboKeyChar( wxKeyEvent& event );	
};

#endif //MAINFRAME_H
