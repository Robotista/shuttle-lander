#include "wxShuttleButtonPicker.hpp"
#include "res/shuttle_pro.png.h"
#include "res/shuttle_express.png.h"
#include <wx/dcbuffer.h>

wxShuttleButtonPicker::wxShuttleButtonPicker(wxWindow* parent)
	: wxPanel(parent, wxID_ANY, wxDefaultPosition, wxSize(700, -1), wxTAB_TRAVERSAL)
{
	Connect(wxEVT_PAINT, wxPaintEventHandler(wxShuttleButtonPicker::OnPanelDisplayPaint), NULL, this);
	Connect(wxEVT_SIZE, wxSizeEventHandler(wxShuttleButtonPicker::OnPanelDisplaySize), NULL, this);
	Connect(wxEVT_MOTION, wxMouseEventHandler(wxShuttleButtonPicker::onMouseMove), NULL, this);
	Connect(wxEVT_LEFT_DOWN, wxMouseEventHandler(wxShuttleButtonPicker::onButtonDown), NULL, this);

	SetBackgroundStyle(wxBG_STYLE_PAINT);

	// initialise wxShuttleButtonPicker with black image
	m_wxDisplayImage.Create(10, 10, true);
}

wxShuttleButtonPicker::~wxShuttleButtonPicker()
{
	Disconnect(wxEVT_PAINT, wxPaintEventHandler(wxShuttleButtonPicker::OnPanelDisplayPaint), NULL, this);
	Disconnect(wxEVT_SIZE, wxSizeEventHandler(wxShuttleButtonPicker::OnPanelDisplaySize), NULL, this);
	Disconnect(wxEVT_MOTION, wxMouseEventHandler(wxShuttleButtonPicker::onMouseMove), NULL, this);
	Disconnect(wxEVT_LEFT_DOWN, wxMouseEventHandler(wxShuttleButtonPicker::onButtonDown), NULL, this);
}

void wxShuttleButtonPicker::addControlElement(const std::vector<wxPoint2DDouble>& elementPolyon, wxPoint2DDouble position, float rotation, 
											  wxString buttonName, wxString buttonCode, ShuttleButtonType shuttleButtonType, wxPoint2DDouble centerOffset)
{
	ControlElement controlElement;
	controlElement.buttonName = buttonName;
	controlElement.buttonCode = buttonCode;
	controlElement.shuttleButtonType = shuttleButtonType;
	controlElement.preselected = false;
	controlElement.selected = false;
	wxPoint2DDouble pointsSum;

	for (const wxPoint2DDouble& point : elementPolyon)
	{
		float s = sin(rotation);
		float c = cos(rotation);

		float x = point.m_x * c - point.m_y * s;
		float y = point.m_x * s + point.m_y * c;

		controlElement.elementPolygonBase.push_back(wxPoint2DDouble(x, y) + position);
		pointsSum += wxPoint2DDouble(x, y) + position;
	}

	controlElement.center = pointsSum / (double)elementPolyon.size();
	controlElement.center += centerOffset;
		
	controlElement.elementPolygonTransformed = controlElement.elementPolygonBase;
	m_controlElements.push_back(std::move(controlElement));
}

void wxShuttleButtonPicker::setShuttleDevice(ShuttleDevice shuttleDevice)
{
	m_shuttleDevice = shuttleDevice;
	m_controlElements.clear();
	m_selectedControlElement = ControlElement(); // Nothing selected

	std::vector<wxPoint2DDouble> jogSmallPoly;
	for (const wxPoint2DDouble& point : JogPoly)
		jogSmallPoly.push_back(point * wxPoint2DDouble(0.63, 0.63));
		
	if (m_shuttleDevice == DEVICE_SHUTTLE_PRO_V1 || m_shuttleDevice == DEVICE_SHUTTLE_PRO_V2)
	{
		wxMemoryInputStream memIStream( shuttle_pro_png, sizeof( shuttle_pro_png ) );	
		m_wxDisplayImage.LoadFile(memIStream, wxBITMAP_TYPE_PNG);
	
		// First line
		addControlElement(keyChickletPoly, wxPoint2DDouble(72, 116), -0.48, "Button 1", "K1", BUTTON_PUSH);
		addControlElement(keyChickletPoly, wxPoint2DDouble(138, 89), -0.18, "Button 2", "K2", BUTTON_PUSH);
		addControlElement(keyChickletPoly, wxPoint2DDouble(209, 80), 0.18, "Button 3", "K3", BUTTON_PUSH);
		addControlElement(keyChickletPoly, wxPoint2DDouble(280, 91), 0.48, "Button 4", "K4", BUTTON_PUSH);

		// Second line
		addControlElement(keyChickletPoly, wxPoint2DDouble(45, 203), -0.72, "Button 5", "K5", BUTTON_PUSH);
		addControlElement(keyChickletPoly, wxPoint2DDouble(105, 157), -0.38, "Button 6", "K6", BUTTON_PUSH);
		addControlElement(keyChickletPoly, wxPoint2DDouble(174, 133), 0, "Button 7", "K7", BUTTON_PUSH);
		addControlElement(keyChickletPoly, wxPoint2DDouble(247, 137), 0.38, "Button 8", "K8", BUTTON_PUSH);
		addControlElement(keyChickletPoly, wxPoint2DDouble(314, 167), 0.72, "Button 9", "K9", BUTTON_PUSH);

		// Banana keys
		addControlElement(keyBananaPoly, wxPoint2DDouble(72, 352), 0, "Button 10", "K10", BUTTON_PUSH);
		addControlElement(keyBananaPoly, wxPoint2DDouble(252, 429), -1.5, "Button 11", "K11", BUTTON_PUSH);
		addControlElement(keyBananaPoly, wxPoint2DDouble(56, 404), -0.14, "Button 12", "K12", BUTTON_PUSH);
		addControlElement(keyBananaPoly, wxPoint2DDouble(258, 469), -1.38, "Button 13", "K13", BUTTON_PUSH);

		// Side (black) buttons
		addControlElement(keySidePoly, wxPoint2DDouble(22, 249), 0, "Button 14", "K14", BUTTON_PUSH, wxPoint2DDouble(-3, 0));
		addControlElement(keySidePoly, wxPoint2DDouble(380, 348), 3.16, "Button 15", "K15", BUTTON_PUSH, wxPoint2DDouble(4, 0));
		
		// Jogging
		addControlElement(jogSmallPoly, wxPoint2DDouble(203, 290), -0.46, "Jog Step Counterclockwise", "JL", BUTTON_JOG);
		addControlElement(jogSmallPoly, wxPoint2DDouble(203, 290), 0.46, "Jog Step Clockwise", "JR", BUTTON_JOG);

		std::vector<wxPoint2DDouble> ShuttleSmallPoly;
		for (const wxPoint2DDouble& point : ShuttlePoly)
			ShuttleSmallPoly.push_back(point * wxPoint2DDouble(0.63, 0.63));

		// Shuttle right
		addControlElement(ShuttleSmallPoly, wxPoint2DDouble(203, 290), 0.17, "Shuttle 1", "S1", BUTTON_SHUTTLE);
		addControlElement(ShuttleSmallPoly, wxPoint2DDouble(203, 290), 0.51, "Shuttle 2", "S2", BUTTON_SHUTTLE);
		addControlElement(ShuttleSmallPoly, wxPoint2DDouble(203, 290), 0.85, "Shuttle 3", "S3", BUTTON_SHUTTLE);
		addControlElement(ShuttleSmallPoly, wxPoint2DDouble(203, 290), 1.19, "Shuttle 4", "S4", BUTTON_SHUTTLE);
		addControlElement(ShuttleSmallPoly, wxPoint2DDouble(203, 290), 1.53, "Shuttle 5", "S5", BUTTON_SHUTTLE);
		addControlElement(ShuttleSmallPoly, wxPoint2DDouble(203, 290), 1.87, "Shuttle 6", "S6", BUTTON_SHUTTLE);
		addControlElement(ShuttleSmallPoly, wxPoint2DDouble(203, 290), 2.21, "Shuttle 7", "S7", BUTTON_SHUTTLE);

		// Shuttle left
		addControlElement(ShuttleSmallPoly, wxPoint2DDouble(203, 290), -0.17, "Shuttle -1", "S-1", BUTTON_SHUTTLE);
		addControlElement(ShuttleSmallPoly, wxPoint2DDouble(203, 290), -0.51, "Shuttle -2", "S-2", BUTTON_SHUTTLE);
		addControlElement(ShuttleSmallPoly, wxPoint2DDouble(203, 290), -0.85, "Shuttle -3", "S-3", BUTTON_SHUTTLE);
		addControlElement(ShuttleSmallPoly, wxPoint2DDouble(203, 290), -1.19, "Shuttle -4", "S-4", BUTTON_SHUTTLE);
		addControlElement(ShuttleSmallPoly, wxPoint2DDouble(203, 290), -1.53, "Shuttle -5", "S-5", BUTTON_SHUTTLE);
		addControlElement(ShuttleSmallPoly, wxPoint2DDouble(203, 290), -1.87, "Shuttle -6", "S-6", BUTTON_SHUTTLE);
		addControlElement(ShuttleSmallPoly, wxPoint2DDouble(203, 290), -2.21, "Shuttle -7", "S-7", BUTTON_SHUTTLE);
	}
	else
	{	
		wxMemoryInputStream memIStream( shuttle_express_png, sizeof( shuttle_express_png ) );	
		m_wxDisplayImage.LoadFile(memIStream, wxBITMAP_TYPE_PNG);
		
		addControlElement(keyXpressBigPoly, wxPoint2DDouble(60, 446), -1.49, "Button 5", "K5", BUTTON_PUSH);
		addControlElement(keyXpressPoly, wxPoint2DDouble(103, 203), -0.72, "Button 6", "K6", BUTTON_PUSH);
		addControlElement(keyXpressPoly, wxPoint2DDouble(264, 81), 0, "Button 7", "K7", BUTTON_PUSH);
		addControlElement(keyXpressPoly, wxPoint2DDouble(467, 92), 0.72, "Button 8", "K8", BUTTON_PUSH);
		addControlElement(keyXpressBigPoly, wxPoint2DDouble(619, 233), 1.49, "Button 9", "K9", BUTTON_PUSH);
		
		addControlElement(JogPoly, wxPoint2DDouble(347, 356), -0.46, "Jog Step Counterclockwise", "JL", BUTTON_JOG);
		addControlElement(JogPoly, wxPoint2DDouble(347, 356), 0.46, "Jog Step Clockwise", "JR", BUTTON_JOG);
		
		addControlElement(ShuttlePoly, wxPoint2DDouble(347, 356), 0.17, "Shuttle 1", "S1", BUTTON_SHUTTLE);
		addControlElement(ShuttlePoly, wxPoint2DDouble(347, 356), 0.51, "Shuttle 2", "S2", BUTTON_SHUTTLE);
		addControlElement(ShuttlePoly, wxPoint2DDouble(347, 356), 0.85, "Shuttle 3", "S3", BUTTON_SHUTTLE);
		addControlElement(ShuttlePoly, wxPoint2DDouble(347, 356), 1.19, "Shuttle 4", "S4", BUTTON_SHUTTLE);
		addControlElement(ShuttlePoly, wxPoint2DDouble(347, 356), 1.53, "Shuttle 5", "S5", BUTTON_SHUTTLE);
		addControlElement(ShuttlePoly, wxPoint2DDouble(347, 356), 1.87, "Shuttle 6", "S6", BUTTON_SHUTTLE);
		addControlElement(ShuttlePoly, wxPoint2DDouble(347, 356), 2.21, "Shuttle 7", "S7", BUTTON_SHUTTLE);
		
		addControlElement(ShuttlePoly, wxPoint2DDouble(347, 356), -0.17, "Shuttle -1", "S-1", BUTTON_SHUTTLE);
		addControlElement(ShuttlePoly, wxPoint2DDouble(347, 356), -0.51, "Shuttle -2", "S-2", BUTTON_SHUTTLE);
		addControlElement(ShuttlePoly, wxPoint2DDouble(347, 356), -0.85, "Shuttle -3", "S-3", BUTTON_SHUTTLE);
		addControlElement(ShuttlePoly, wxPoint2DDouble(347, 356), -1.19, "Shuttle -4", "S-4", BUTTON_SHUTTLE);
		addControlElement(ShuttlePoly, wxPoint2DDouble(347, 356), -1.53, "Shuttle -5", "S-5", BUTTON_SHUTTLE);
		addControlElement(ShuttlePoly, wxPoint2DDouble(347, 356), -1.87, "Shuttle -6", "S-6", BUTTON_SHUTTLE);
		addControlElement(ShuttlePoly, wxPoint2DDouble(347, 356), -2.21, "Shuttle -7", "S-7", BUTTON_SHUTTLE);
	}

	Refresh();
	wxSizeEvent event;
	OnPanelDisplaySize(event);
}

bool wxShuttleButtonPicker::checkIsInPolygon(const std::vector<wxPoint2DDouble>& polygon, wxPoint2DDouble testPoint )
{
	int nPoints = polygon.size();
	int j = -999;
	bool locatedInPolygon = false;
	for (int i = 0; i<(nPoints); i++)
	{
		if (i == (nPoints - 1))
			j = 0;
		else
			j = i + 1;

		float vertY_i = (float)polygon[i].m_y;
		float vertX_i = (float)polygon[i].m_x;
		float vertY_j = (float)polygon[j].m_y;
		float vertX_j = (float)polygon[j].m_x;
		float testX = (float)testPoint.m_x;
		float testY = (float)testPoint.m_y;

		bool belowLowY = vertY_i > testY;
		bool belowHighY = vertY_j > testY;
		bool withinYsEdges = belowLowY != belowHighY;

		if (withinYsEdges)
		{
			float slopeOfLine = (vertX_j - vertX_i) / (vertY_j - vertY_i);
			float pointOnLine = (slopeOfLine* (testY - vertY_i)) + vertX_i;
			bool isLeftToLine = testX < pointOnLine;

			if (isLeftToLine)
				locatedInPolygon = !locatedInPolygon;
		}
	}

	return locatedInPolygon;
}

void wxShuttleButtonPicker::OnPanelDisplayPaint(wxPaintEvent&)
{
	wxAutoBufferedPaintDC dc(this);

	dc.SetBackground(wxBrush(*wxBLACK_BRUSH));
	dc.SetBrush(wxColour(200, 200, 200));	
	dc.SetPen(wxColour(110, 110, 110));	
	dc.Clear();

	int win_width = this->GetClientSize().GetX();
	int win_height = this->GetClientSize().GetY();

	// if the wxVideoDisplay component is too small, skip the drawing
	if (win_width < 10 || win_height < 10)
		return;

	if (!dc.IsOk() || !m_wxDisplayImage.IsOk())
		return;

	wxGraphicsContext *gc = wxGraphicsContext::Create(dc);

	wxBitmap bmc = wxBitmap(m_wxDisplayImage.Scale(m_frameSize.x, m_frameSize.y, wxIMAGE_QUALITY_BICUBIC));
	dc.DrawBitmap(bmc, m_framePos.x, m_framePos.y);

	gc->SetBrush(wxBrush(wxColour(0, 200, 128, 255)));
	int fontSize = m_frameSize.y / 42;
	wxFont font(std::max(fontSize, 2), wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false);
	dc.SetFont(font);
		
	for (ControlElement& controlElement : m_controlElements)
	{
		wxColour selectedColorBrush = controlElement.shuttleButtonType == BUTTON_SHUTTLE ? wxColour(220, 220, 220, 200) : wxColour(0, 0, 128, 200);
		wxColour preselectedColorBrush = controlElement.shuttleButtonType == BUTTON_SHUTTLE ? wxColour(200, 200, 200, 100) : wxColour(0, 0, 128, 100);
		
		wxColour selectedColorFont = controlElement.shuttleButtonType == BUTTON_SHUTTLE ? wxColour(0, 0, 128, 200) : wxColour(220, 220, 220);
		wxColour unselectedColorFont = controlElement.shuttleButtonType == BUTTON_SHUTTLE ? wxColour(220, 220, 220) : wxColour(0, 0, 128, 200);
		
		if (controlElement.selected)
		{
			gc->SetBrush(wxBrush(selectedColorBrush));
			dc.SetTextForeground(selectedColorFont);
		}
		else if (controlElement.preselected)
		{
			gc->SetBrush(wxBrush(preselectedColorBrush));
			dc.SetTextForeground(wxColour(220, 220, 220));
		}
		else
		{
			dc.SetTextForeground(unselectedColorFont);
		}
		
		for (int i = 0; i < controlElement.elementPolygonBase.size(); i++)
			controlElement.elementPolygonTransformed[i] = controlElement.elementPolygonBase[i] * wxPoint2DDouble(m_frameScale, m_frameScale) + wxPoint2DDouble(m_framePos.x, m_framePos.y);

		if (controlElement.selected || controlElement.preselected)
			gc->DrawLines(controlElement.elementPolygonTransformed.size(), controlElement.elementPolygonTransformed.data(), wxODDEVEN_RULE);
		
		wxPoint2DDouble center = controlElement.center * wxPoint2DDouble(m_frameScale, m_frameScale) + wxPoint2DDouble(m_framePos.x, m_framePos.y);
		
		if (controlElement.buttonLabel.empty())
		{
			dc.DrawCircle(wxPoint(center.m_x, center.m_y), 2);
		}
		else
		{			
			wxSize extent = dc.GetTextExtent(controlElement.buttonLabel);
			dc.DrawText(controlElement.buttonLabel, wxPoint(center.m_x - extent.GetWidth() / 2, center.m_y - extent.GetHeight() / 2));
		}
	}
	
	delete gc;
}

// handling of resizing of wxShuttleButtonPicker component
void wxShuttleButtonPicker::OnPanelDisplaySize(wxSizeEvent&)
{
	int imgOriginalWidth = m_wxDisplayImage.GetSize().GetX();
	int imgOriginalHeight = m_wxDisplayImage.GetSize().GetY();
	int win_width = this->GetClientSize().GetX();
	int win_height = this->GetClientSize().GetY();
	int new_img_width = (imgOriginalWidth * win_height) / imgOriginalHeight;
	int new_img_height = (imgOriginalHeight * win_width) / imgOriginalWidth;
	int width_diff = (win_width - new_img_width) / 2;
	int height_diff = (win_height - new_img_height) / 2;

	m_framePos.x = 0;
	m_framePos.y = 0;
	m_frameSize.x = 0;
	m_frameSize.y = 0;
	if (height_diff >= 0)
	{
		m_framePos.x = 0;
		m_framePos.y = height_diff;
		m_frameSize.x = win_width;
		m_frameSize.y = new_img_height;
	}
	else
	{
		m_framePos.x = width_diff;
		m_framePos.y = 0;
		m_frameSize.x = new_img_width;
		m_frameSize.y = win_height;
	}

	m_frameScale = m_frameSize.x / (float)imgOriginalWidth;
	Refresh();
}

void wxShuttleButtonPicker::onButtonDown(wxMouseEvent& event)
{
	int mouseX = event.GetPosition().x;
	int mouseY = event.GetPosition().y;

	ControlElement* selectedControlElement = nullptr;
	ControlElement* unselectedControlElement = nullptr;
	for (ControlElement& controlElement : m_controlElements)
	{
		bool selected = checkIsInPolygon(controlElement.elementPolygonTransformed, wxPoint2DDouble(mouseX, mouseY));
		if (selected && !controlElement.selected)
			selectedControlElement = &controlElement;

		if (!selected && controlElement.selected)
			unselectedControlElement = &controlElement;
	}
	
	if (selectedControlElement)
	{
		if (unselectedControlElement)
			unselectedControlElement->selected = false;
		
		selectedControlElement->selected = true;
		m_selectedControlElement = *selectedControlElement;
		m_observer->OnShuttleButtonSelected(m_selectedControlElement);
		Refresh();
	}	
}

void wxShuttleButtonPicker::onMouseMove(wxMouseEvent& event)
{
	int mouseX = event.GetPosition().x;
	int mouseY = event.GetPosition().y;

	for (ControlElement& controlElement : m_controlElements)
	{	
		bool preselected = checkIsInPolygon(controlElement.elementPolygonTransformed, wxPoint2DDouble(mouseX, mouseY));
		if (preselected != controlElement.preselected)
			Refresh();

		controlElement.preselected = preselected;
	}
}

void wxShuttleButtonPicker::setButtonLabels(const std::map<wxString, wxString>& buttonLabels)
{	
	std::map<wxString, wxString> buttonLabel;
	for (ControlElement& controlElement : m_controlElements)
	{		
		std::map<wxString, wxString>::const_iterator it = buttonLabels.find(controlElement.buttonCode);
		if (it != buttonLabels.end())
			controlElement.buttonLabel = it->second; // We have a label for this button
		else
			controlElement.buttonLabel = ""; // We dont have a label
	}
	Refresh();
}
