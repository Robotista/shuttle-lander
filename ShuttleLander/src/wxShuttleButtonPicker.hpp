#ifndef WXSHUTTLEBUTTONPICKER_H
#define WXSHUTTLEBUTTONPICKER_H

#include <wx/image.h>
#include <wx/panel.h>
#include <wx/graphics.h>
#include <vector>
#include <map>

enum ShuttleDevice
{
	DEVICE_SHUTTLE_PRO_V1,
	DEVICE_SHUTTLE_PRO_V2,
	DEVICE_SHUTTLE_EXPRESS
};

enum ShuttleButtonType
{
	BUTTON_PUSH,
	BUTTON_JOG,
	BUTTON_SHUTTLE
};

enum VirtualKeyBehavior
{
	BEHAVIOR_TAP,
	BEHAVIOR_HOLD,
	BEHAVIOR_REPEAT
};


struct ControlElement
{
	wxPoint2DDouble					center;
	wxString						buttonName;
	wxString						buttonCode;
	wxString						buttonLabel;
	std::vector<wxPoint2DDouble>	elementPolygonBase;
	std::vector<wxPoint2DDouble>	elementPolygonTransformed; // Polygon of the control element after scaling the window
	bool							preselected;
	bool							selected;
	ShuttleButtonType 				shuttleButtonType;
};

const std::vector<wxPoint2DDouble> keyChickletPoly = {
	wxPoint2DDouble(10, 0),
	wxPoint2DDouble(45, 0),
	wxPoint2DDouble(52, 4),
	wxPoint2DDouble(55, 10),
	wxPoint2DDouble(55, 29),
	wxPoint2DDouble(52, 35),
	wxPoint2DDouble(45, 39),
	wxPoint2DDouble(10, 39),
	wxPoint2DDouble(3, 35),
	wxPoint2DDouble(0, 29),
	wxPoint2DDouble(0, 10),
	wxPoint2DDouble(3, 4)
};

const std::vector<wxPoint2DDouble> keyBananaPoly = {
	wxPoint2DDouble(17, 0),
	wxPoint2DDouble(23, 3),
	wxPoint2DDouble(46, 32),
	wxPoint2DDouble(75, 55),
	wxPoint2DDouble(77, 61),
	wxPoint2DDouble(76, 67),
	wxPoint2DDouble(69, 77),
	wxPoint2DDouble(63, 78),
	wxPoint2DDouble(57, 77),
	wxPoint2DDouble(39, 64),
	wxPoint2DDouble(14, 39),
	wxPoint2DDouble(1, 19),
	wxPoint2DDouble(0, 14),
	wxPoint2DDouble(2, 8),
	wxPoint2DDouble(10, 1)
};

const std::vector<wxPoint2DDouble> keySidePoly = {
	wxPoint2DDouble(22, 2),
	wxPoint2DDouble(33, 3),
	wxPoint2DDouble(41, 7),
	wxPoint2DDouble(44, 16),
	wxPoint2DDouble(40, 46),
	wxPoint2DDouble(45, 80),
	wxPoint2DDouble(45, 87),
	wxPoint2DDouble(40, 92),
	wxPoint2DDouble(25, 97),
	wxPoint2DDouble(15, 95),
	wxPoint2DDouble(9, 89),
	wxPoint2DDouble(4, 62),
	wxPoint2DDouble(3, 27),
	wxPoint2DDouble(8, 11),
	wxPoint2DDouble(14, 5)
};

const std::vector<wxPoint2DDouble> keyXpressPoly = {
	wxPoint2DDouble(17, 8),
	wxPoint2DDouble(58, 3),
	wxPoint2DDouble(107, 3),
	wxPoint2DDouble(157, 8),
	wxPoint2DDouble(168, 19),
	wxPoint2DDouble(169, 31),
	wxPoint2DDouble(149, 66),
	wxPoint2DDouble(143, 70),
	wxPoint2DDouble(135, 68),
	wxPoint2DDouble(114, 64),
	wxPoint2DDouble(86, 62),
	wxPoint2DDouble(54, 64),
	wxPoint2DDouble(33, 69),
	wxPoint2DDouble(26, 70),
	wxPoint2DDouble(21, 68),
	wxPoint2DDouble(1, 33),
	wxPoint2DDouble(3, 18)
};

const std::vector<wxPoint2DDouble> keyXpressBigPoly = {
	wxPoint2DDouble(11, 14),
	wxPoint2DDouble(44, 6),
	wxPoint2DDouble(81, 1),
	wxPoint2DDouble(129, 1),
	wxPoint2DDouble(171, 7),
	wxPoint2DDouble(200, 16),
	wxPoint2DDouble(209, 24),
	wxPoint2DDouble(208, 39),
	wxPoint2DDouble(194, 66),
	wxPoint2DDouble(184, 72),
	wxPoint2DDouble(171, 70),
	wxPoint2DDouble(137, 63),
	wxPoint2DDouble(103, 61),
	wxPoint2DDouble(68, 64),
	wxPoint2DDouble(42, 71),
	wxPoint2DDouble(30, 72),
	wxPoint2DDouble(19, 66),
	wxPoint2DDouble(2, 33),
	wxPoint2DDouble(3, 22)
};

const std::vector<wxPoint2DDouble> JogPoly = {
	wxPoint2DDouble(0, 0),
	wxPoint2DDouble(-49, -101),
	wxPoint2DDouble(-32, -107),
	wxPoint2DDouble(-11, -112),
	wxPoint2DDouble(11, -112),
	wxPoint2DDouble(32, -107),
	wxPoint2DDouble(49, -101)
};

const std::vector<wxPoint2DDouble> ShuttlePoly = {
	wxPoint2DDouble(-25, -150),
	wxPoint2DDouble(0, -152),
	wxPoint2DDouble(25, -150),
	wxPoint2DDouble(20, -118),
	wxPoint2DDouble(-20, -118)
};

class ShuttleButtonPickerObserver
{
public:
	virtual void OnShuttleButtonSelected(ControlElement controlElement) = 0;
};

class wxShuttleButtonPicker : public wxPanel
{

public:
	wxShuttleButtonPicker(wxWindow* parent);
	~wxShuttleButtonPicker();
	
 	void setShuttleDevice(ShuttleDevice shuttleDevice);
	void setObserver(ShuttleButtonPickerObserver* observer) { m_observer = observer; }
	void setButtonLabels(const std::map<wxString, wxString>& buttonLabels);
	ControlElement& getSelectedControlElement() { return m_selectedControlElement; }

private:
	wxImage							m_wxDisplayImage;
	float							m_frameScale;
	wxPoint							m_framePos;
	wxPoint							m_frameSize;
	ShuttleDevice					m_shuttleDevice;
	std::vector<ControlElement>		m_controlElements;
	ControlElement					m_selectedControlElement;
	ShuttleButtonPickerObserver*	m_observer;

	void addControlElement(const std::vector<wxPoint2DDouble>& elementPolyon, wxPoint2DDouble position, float rotation, wxString buttonLabel, wxString buttonCode, ShuttleButtonType shuttleButtonType,
						   wxPoint2DDouble centerOffset = wxPoint2DDouble(0.0, 0.0));
	bool checkIsInPolygon(const std::vector<wxPoint2DDouble>& polygon, wxPoint2DDouble testPoint);

	// Events
	void OnPanelDisplayPaint(wxPaintEvent& event);
	void OnPanelDisplaySize(wxSizeEvent& event);
	void onButtonDown(wxMouseEvent& event);
	void onMouseMove(wxMouseEvent& event);

	void setDefaultScale();
};
#endif // WXSHUTTLEBUTTONPICKER_H
