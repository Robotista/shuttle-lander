﻿#include "MainFrame.hpp"
#include "wx/log.h"
#include "wx/graphics.h"
#include <wx/aboutdlg.h>
#include <wx/msgdlg.h>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <dirent.h>
#include <linux/input.h>
#include "VirtualKeys.hpp"
#include "DefaultShuttleRc.hpp"

#define APP_NAME "Shuttle Lander"

MainFrame::MainFrame(wxWindow* parent)
	:MainFrame_design(parent)
{
	SetTitle(wxString(APP_NAME));

	m_wxShuttleButtonPicker->setObserver(this);
	m_editAppDialog = new EditAppDialog(this);
	
	loadShuttlerc();
	
	wxString envPath;
	if (wxGetEnv("XDG_SESSION_TYPE", &envPath) && envPath == "wayland")
	{
		m_comboApplication->Append(wxT("This is unavailable on Wayland"));
		m_comboApplication->SetSelection(0);
		m_comboApplication->Enable(false);
		m_btnEdit->Enable(false);
	}
	else
	{
		for (const TranslationSection& translationSection : m_translationSections)
			m_comboApplication->Append(translationSection.name);
		
		m_comboApplication->Append(wxT("+ Add New"));
		
		if (m_translationSections.size() > 0)
			m_comboApplication->SetSelection(0);
	}
	
	m_comboKey->Append(""); // Append the unused, empty key code
	for (const VirtualKey& virtualKey : VirtualKeys)
		m_comboKey->Append(virtualKey.keyName);
	
	m_comboModifierKey->Append(""); // Append the unused, empty modifier key code
	for (const VirtualKey& virtualModifierKey : VirtualModifierKeys)
		m_comboModifierKey->Append(virtualModifierKey.keyName);
	
	m_comboBehavior->Disable();	
	m_comboKey->Disable();	
	m_comboModifierKey->Disable();	
	m_panelOptions->Hide();
	m_buttonApply->Hide();
	CallAfter([&] { m_txtRepeatTime->Hide(); m_spinRepeatTime->Hide(); }); // Hide the controls, but after they are inited and sized properly
		
	ShuttleDevice shuttleDevice;
	wxString shuttleDeviceName;	
	if (getShuttleDevice(shuttleDevice, shuttleDeviceName))
	{
		m_wxShuttleButtonPicker->setShuttleDevice(shuttleDevice);
		m_txtDevice->SetLabel(shuttleDeviceName);
		m_panelOptions->Show();
		m_buttonApply->Show();
		m_btnRefreshDevices->Hide();
		refreshButtonLabels();
	}

	// Manual window resizing - workaround for wxWidgets bug where wxStaticText is not sized properly for non-default font sizes
	SetSize(675, 650);
}

////////////////////////MAIN MENU//////////////////////////////////
void MainFrame::OnMenuAbout(wxCommandEvent& WXUNUSED(event))
{
 	wxAboutDialogInfo info;
 	info.SetName(_("Shuttle Lander"));
 	info.SetVersion(_("1.2.0"));
	info.SetDescription(_("This program allows you to convert the control inputs from Shuttle jog controllers into virtual keyboard keypresses."
	" This allows you to use the Shuttle controller with the applications that are not natively supporting it."));
 	info.SetCopyright(wxT("(C) Robotista <robotista@email.cz>"));
 
 	wxAboutBox(info);
}
	
///////////////////////////////////////////////////////////////////

std::string MainFrame::getTokenFlag(const std::string &token)
{
	std::size_t slash = token.find("/");
	if (slash == std::string::npos)
		return "";
	
	return token.substr(slash + 1, 1);
}

std::string MainFrame::getTokenKey(const std::string &token)
{
	std::size_t slash = token.find("/");
	if (slash == std::string::npos)
		return "";
	
	return token.substr(0, slash);
}

int MainFrame::strToInt(const std::string &input, int defaultValue)
{
	try
	{
		int result = std::stoi(input);	
		return result;
	}
	catch(std::exception &err)
	{
		return defaultValue;
	}
}

wxString MainFrame::getConfigPath()
{
	wxString configPath;
	wxString xdgConfigHome;
	wxGetEnv("XDG_CONFIG_HOME", &xdgConfigHome);
	
	wxString home;
	wxGetEnv("HOME", &home);
	if(!xdgConfigHome.IsEmpty())
		configPath = xdgConfigHome + "/ShuttleLander";
	else
		configPath = home + "/.config/ShuttleLander";
	
	// Create the folder, if not exists
	if (!wxDirExists(configPath))
	{
		if(!wxMkdir(configPath))
		{			
			std::cerr << "Cannot create a new folder under: " << configPath << std::endl;
			exit(1);
		}
	}
	
	// Create the shuttlerc file if not exists, for the current user
	configPath += "/shuttlerc";
	if (!wxFileExists(configPath))
	{
		std::ofstream outfile;
		outfile.open(configPath);
		outfile << defaultShuttleRc << std::endl;
		outfile.close();
	}
		
	return configPath;
}

void MainFrame::loadShuttlerc()
{
	wxString configPath = getConfigPath();
	std::ifstream configFile(configPath);
	
	TranslationSection translationSection;
	std::string line;
	while (std::getline(configFile, line))
	{ 
		// Remove leading spaces
		line.erase(0, line.find_first_not_of(" \t"));
		
		
		// Comment or empty (skip)
		if (line.rfind("#", 0) == 0 || line.empty())
			continue;
		
		// Debug flag (skip)
		if (line.rfind("DEBUG_REGEX", 0) == 0 || line.rfind("DEBUG_STROKES", 0) == 0)
			continue;
		
		// New translation section
		if (line.rfind("[", 0) == 0)
		{
			if (!translationSection.name.IsEmpty())
				m_translationSections.push_back(translationSection);
			
			std::size_t close = line.find("]");
			std::string name = line.substr(1, close - 1);
			std::string regexp = line.substr(close + 1);
			regexp.erase(0, regexp.find_first_not_of(" \t")); // Remove leading spaces
			translationSection.name = name;
			translationSection.regex = regexp;
			translationSection.keyMap.clear();
			continue;
		}
		
		// Shuttle stroke line
		std::stringstream ss(line);
		ShuttleStroke shuttleStroke;
		shuttleStroke.modifierKeyCode = -1;
		std::string buttonCode;
		std::string previousToken;
		std::string token;
		while (ss >> token)
		{
			//std::cout << token << std::endl;
			
			// First token on the line is Shuttle device's button code
			if (buttonCode.empty())
			{
				buttonCode = token;
				continue;
			}
			
			// Token starting with "#", thats a comment, we are finish with this line
			if (token.rfind("#", 0) == 0)
				break;
			
			// Was the key from previous token a Modifier key? (key which is pressed, but not released before next key is pressed)
			if ((getTokenFlag(previousToken) == "D" || getTokenFlag(previousToken) == "H") && getTokenKey(previousToken) != getTokenKey(token))
				shuttleStroke.modifierKeyCode = strToInt(getTokenKey(previousToken), -1); // Was a modifier
				
			// Key hold
			if (getTokenFlag(token).empty())
			{
				shuttleStroke.keyCode = strToInt(token, -1);
				shuttleStroke.virtualKeyBehavior = BEHAVIOR_HOLD;
			}
				
			// Key tap
			if (getTokenFlag(previousToken) == "D" && getTokenFlag(token) == "U" & getTokenKey(previousToken) == getTokenKey(token))
			{
				shuttleStroke.keyCode = strToInt(getTokenKey(token), -1);
				shuttleStroke.virtualKeyBehavior = BEHAVIOR_TAP;
			}
				
			// Key repeat
			if (getTokenFlag(token) == "R")
			{
				std::size_t slashR = token.find("/R");				
				std::string repeatTime = token.substr(slashR + 2);
				
				shuttleStroke.keyCode = strToInt(getTokenKey(token), -1);
				shuttleStroke.repeatTime = strToInt(repeatTime, -1);
				shuttleStroke.virtualKeyBehavior = BEHAVIOR_REPEAT;
			}
				
			previousToken = token;
		}
		
		if (!buttonCode.empty())
			translationSection.keyMap.insert(std::pair<wxString, ShuttleStroke>(buttonCode, shuttleStroke));
	}
	
	if (!translationSection.name.IsEmpty())
		m_translationSections.push_back(translationSection);
}

int MainFrame::checkSupported(int fd)
{
	int supportedDevices[3][2] =
	{
		{0x0b33, 0x0010}, // Contour Design ShuttlePro v1
		{0x0b33, 0x0030}, // Contour Design ShuttlePro v2
		{0x0b33, 0x0020} // Contour Design ShuttleXpress
	};
	
	short devinfo[4];	
	if (ioctl(fd, EVIOCGID, &devinfo))
	{
		perror("evdev ioctl");
		return -1;
	}
		
	for (int i = 0; i < 3 ; i++) 
	{
		if (supportedDevices[i][0] == devinfo[1] && supportedDevices[i][1] == devinfo[2])
			return i;
	}
		
	return -1;
}

bool MainFrame::isShuttleDevice(std::string devname, int& shuttleDevice)
{
	int fd = open(devname.c_str(), O_RDONLY);
	if (fd < 0)
		return 0;
	else
	{
		shuttleDevice = checkSupported(fd);
		if (shuttleDevice >= 0)
		{
			close(fd);
			return true;
		}
		close(fd);
		return false;
	}
}

bool MainFrame::getShuttleDevice(ShuttleDevice& shuttleDevice, wxString& shuttleDeviceName)
{
	std::string devDirStr = "/dev/input/by-id";
	DIR* input_dir = opendir(devDirStr.c_str());
	if (input_dir == NULL)
	{
		devDirStr = "/dev/input";
		input_dir = opendir(devDirStr.c_str());
	}
	
	struct dirent *dir;
	if (input_dir)
	{
		while ((dir = readdir(input_dir)) != NULL)
		{
			std::string fullPath = devDirStr + "/" + std::string(dir->d_name);
			int shuttleDevRaw = -1;
			if (dir->d_name[0] != '.' && isShuttleDevice(fullPath, shuttleDevRaw))
			{
				shuttleDevice = static_cast<ShuttleDevice>(shuttleDevRaw);
				switch(shuttleDevice)
				{
					case DEVICE_SHUTTLE_PRO_V1 : shuttleDeviceName = "Shuttle Pro v1"; break;
					case DEVICE_SHUTTLE_PRO_V2 : shuttleDeviceName = "Shuttle Pro v2"; break;
					case DEVICE_SHUTTLE_EXPRESS : shuttleDeviceName = "Shuttle Express"; break;
				}
				return true;
			}
		}
		closedir(input_dir);
	}
	return false;
}

void MainFrame::OnClose(wxCloseEvent& WXUNUSED(event))
{
	this->Destroy();
}

void MainFrame::OnButtonApply(wxCommandEvent& WXUNUSED(event))
{
	wxString configPath = getConfigPath();
	
	std::ofstream outfile;
	outfile.open(configPath);
	
	for (const TranslationSection& translationSection : m_translationSections)
	{
		// Section header
		outfile << "[" << translationSection.name << "] " << translationSection.regex << std::endl;
		
		// Shuttle Buttons
		for (auto const& key : translationSection.keyMap)
		{
			const ShuttleStroke& shuttleStroke = key.second;
			
			if (shuttleStroke.keyCode == -1)
				continue;
			
			std::string modifierKey;
			switch(shuttleStroke.virtualKeyBehavior)
			{
				case BEHAVIOR_TAP:
					modifierKey = shuttleStroke.modifierKeyCode != -1 ? std::to_string(shuttleStroke.modifierKeyCode) + "/D" + " " : "";
					outfile << key.first << " " << modifierKey << shuttleStroke.keyCode << "/D " << shuttleStroke.keyCode << "/U " << std::endl;
					break;
				case BEHAVIOR_HOLD:
					modifierKey = shuttleStroke.modifierKeyCode != -1 ? std::to_string(shuttleStroke.modifierKeyCode) + "/H" + " "  : "";
					outfile << key.first << " " << modifierKey << shuttleStroke.keyCode << std::endl;
					break;
				case BEHAVIOR_REPEAT:
					modifierKey = shuttleStroke.modifierKeyCode != -1 ? std::to_string(shuttleStroke.modifierKeyCode) + "/H" + " "  : "";
					outfile << key.first << " " << modifierKey << shuttleStroke.keyCode << "/R" << shuttleStroke.repeatTime << std::endl;
					break;
			}
		}
		outfile << std::endl;
		//outfile << "[" << translationSection.name << "] " << translationSection.regex << std::endl;
	}
	
	outfile.close();
}

void MainFrame::OnButtonAppEdit(wxCommandEvent& WXUNUSED(event))
{
	int selectedApp = m_comboApplication->GetSelection();	
	wxString appName = m_translationSections[selectedApp].name;
	wxString regExp  = m_translationSections[selectedApp].regex;
	m_editAppDialog->preset(false, appName, regExp);
	m_editAppDialog->ShowModal();
}

void MainFrame::OnButtonAppRemove(wxCommandEvent& WXUNUSED(event))
{
	int selectedApp = m_comboApplication->GetSelection();
	wxString appName = m_translationSections[selectedApp].name;
	wxMessageDialog *dialog = new wxMessageDialog(NULL, wxT("Remove target application " + appName + "?"), wxT("Confirm the deleting"), wxYES_NO | wxNO_DEFAULT | wxICON_QUESTION);
	if (dialog->ShowModal() != wxID_YES)
		return;

	m_comboApplication->SetSelection(selectedApp - 1);
	m_comboApplication->Delete(selectedApp);
	m_translationSections.erase(m_translationSections.begin() + selectedApp);
	
	// Simulate the selection event to refresh
	wxCommandEvent commandEvent;
	OnComboApplication(commandEvent);	
}

void MainFrame::OnButtonRefreshDevices(wxCommandEvent& WXUNUSED(event))
{
	ShuttleDevice shuttleDevice;
	wxString shuttleDeviceName;	
	if (getShuttleDevice(shuttleDevice, shuttleDeviceName))
	{
		m_wxShuttleButtonPicker->setShuttleDevice(shuttleDevice);
		m_txtDevice->SetLabel(shuttleDeviceName);
		m_btnRefreshDevices->Hide();
		m_panelOptions->Show();
		m_buttonApply->Show();
		refreshButtonLabels();
	}
	else
	{
		m_txtDevice->SetLabel("No device detected");
		m_btnRefreshDevices->Show();
		m_buttonApply->Hide();
	}
	
	Layout();
}

void MainFrame::refreshButtonLabels()
{
	int selectedApp = m_comboApplication->GetSelection();
	std::map<wxString, wxString> buttonLabels;
	for (auto const& key : m_translationSections[selectedApp].keyMap)
	{
		const ShuttleStroke& shuttleStroke = key.second;
		
		if (shuttleStroke.keyCode == -1)
			continue;
		
		for (const VirtualKey& virtualKey : VirtualKeys)
			if (virtualKey.uinputCode == shuttleStroke.keyCode)
			{
				buttonLabels[key.first] = virtualKey.keyName.SubString(0, 1);
				break;
			}
	}
	m_wxShuttleButtonPicker->setButtonLabels(buttonLabels);
}

void MainFrame::OnComboApplication( wxCommandEvent& WXUNUSED(event))
{
	// "+ Add New App" selected
	if (m_comboApplication->GetSelection() + 1 >= m_comboApplication->GetCount())
	{
		m_editAppDialog->preset(true, "", "");
		m_editAppDialog->ShowModal();
		m_comboApplication->SetSelection(m_comboApplication->GetCount()-2);
		return;
	}
	
	if (m_comboApplication->GetSelection() == 0)
	{
		m_btnEdit->Disable();
		m_btnRemove->Disable();
	}
	else
	{
		m_btnEdit->Enable();
		m_btnRemove->Enable();
	}
	
	// Just force everything to refresh
	ControlElement& controlElement = m_wxShuttleButtonPicker->getSelectedControlElement();
	OnShuttleButtonSelected(controlElement);
	refreshButtonLabels();
}

void MainFrame::OnComboKeyBehavor(wxCommandEvent& event)
{
	ControlElement& controlElement = m_wxShuttleButtonPicker->getSelectedControlElement();
	if (controlElement.buttonCode.IsEmpty())
		return;
	
	int keyBehavior = event.GetSelection();	
	m_txtRepeatTime->Show(keyBehavior == BEHAVIOR_REPEAT);
	m_spinRepeatTime->Show(keyBehavior == BEHAVIOR_REPEAT);
	
	int selectedApp = m_comboApplication->GetSelection();
	std::map<wxString, ShuttleStroke>& keyMap = m_translationSections[selectedApp].keyMap;
	
	if ( keyMap.find(controlElement.buttonCode) != keyMap.end() )
		keyMap[controlElement.buttonCode].virtualKeyBehavior = static_cast<VirtualKeyBehavior>(keyBehavior);
}

void MainFrame::OnComboKey(wxCommandEvent& event)
{
	ControlElement& controlElement = m_wxShuttleButtonPicker->getSelectedControlElement();
	if (controlElement.buttonCode.IsEmpty())
		return;
	
	int keyCode = -1;
	for (const VirtualKey& virtualKey : VirtualKeys)
		if (virtualKey.keyName == event.GetString())
		{
			keyCode = virtualKey.uinputCode;
			break;
		}
	
	int modifierKeyCode = -1;
	for (const VirtualKey& virtualModifierKey : VirtualModifierKeys)
		if (virtualModifierKey.keyName == m_comboModifierKey->GetStringSelection())
		{
			modifierKeyCode = virtualModifierKey.uinputCode;
			break;
		}
	
	int selectedApp = m_comboApplication->GetSelection();
	std::map<wxString, ShuttleStroke>& keyMap = m_translationSections[selectedApp].keyMap;
	
	// Insert the key map entry if missing
	if ( keyMap.find(controlElement.buttonCode) == keyMap.end() )
	{
		ShuttleStroke shuttleStroke;
		shuttleStroke.keyCode = -1;
		shuttleStroke.modifierKeyCode = modifierKeyCode;
		shuttleStroke.repeatTime = m_spinRepeatTime->GetValue();
		shuttleStroke.virtualKeyBehavior = static_cast<VirtualKeyBehavior>(m_comboBehavior->GetSelection());
		keyMap[controlElement.buttonCode] = shuttleStroke;
	}
	keyMap[controlElement.buttonCode].keyCode = keyCode;
	refreshButtonLabels();
}

void MainFrame::OnComboModifierKey(wxCommandEvent& event)
{
	ControlElement& controlElement = m_wxShuttleButtonPicker->getSelectedControlElement();
	if (controlElement.buttonCode.IsEmpty())
		return;
	
	int modifierKeyCode = -1;
	for (const VirtualKey& virtualModifierKey : VirtualModifierKeys)
		if (virtualModifierKey.keyName == event.GetString())
		{
			modifierKeyCode = virtualModifierKey.uinputCode;
			break;
		}
		
	int selectedApp = m_comboApplication->GetSelection();
	std::map<wxString, ShuttleStroke>& keyMap = m_translationSections[selectedApp].keyMap;
	
	if ( keyMap.find(controlElement.buttonCode) != keyMap.end() )
		keyMap[controlElement.buttonCode].modifierKeyCode = modifierKeyCode;
}

void MainFrame::OnSpinRepeatTime( wxSpinEvent& event )
{
	ControlElement& controlElement = m_wxShuttleButtonPicker->getSelectedControlElement();
	if (controlElement.buttonCode.IsEmpty())
		return;
	
	int selectedApp = m_comboApplication->GetSelection();
	std::map<wxString, ShuttleStroke>& keyMap = m_translationSections[selectedApp].keyMap;
	
	if ( keyMap.find(controlElement.buttonCode) != keyMap.end() )
		keyMap[controlElement.buttonCode].repeatTime = event.GetValue();
}

void MainFrame::OnComboKeyChar( wxKeyEvent& event )
{ 
	int keyCode = event.GetKeyCode();
	
	for (const VirtualKey& virtualKey : VirtualKeys)
		if (virtualKey.asciiCode == keyCode)
		{
			m_comboKey->SetStringSelection(virtualKey.keyName);
			wxCommandEvent commandEvent;
			commandEvent.SetString(virtualKey.keyName);
			OnComboKey(commandEvent);
			break;
		}
}

void MainFrame::OnShuttleButtonSelected(ControlElement controlElement)
{
	int selectedApp = m_comboApplication->GetSelection();
	m_txtControlElement->SetLabel(controlElement.buttonName);
	
	// Adapt the GUI to the type of selected Shuttle button
	switch (controlElement.shuttleButtonType)
	{
		case BUTTON_JOG:
			m_comboBehavior->Clear();
			m_comboBehavior->Append( "Tap the key" );
			m_comboBehavior->SetSelection(0);
			break;
		default:
			m_comboBehavior->Clear();
			m_comboBehavior->Append( "Tap the key" );
			m_comboBehavior->Append( "Hold key until Shuttle button released" );
			m_comboBehavior->Append( "Repeat key until Shuttle button released" );
			m_comboBehavior->SetSelection(0);
			break;
	}
	
	m_comboBehavior->Enable();
	m_comboKey->Enable();
	m_comboModifierKey->Enable();

	m_txtRepeatTime->Show(m_comboBehavior->GetSelection() == BEHAVIOR_REPEAT);
	m_spinRepeatTime->Show(m_comboBehavior->GetSelection() == BEHAVIOR_REPEAT);
	
	std::map<wxString, ShuttleStroke>& keyMap = m_translationSections[selectedApp].keyMap;
	if (keyMap.find(controlElement.buttonCode) == keyMap.end() )
	{
		m_comboKey->SetSelection(0);
		m_comboModifierKey->SetSelection(0);
		return;
	}	
	
	int keyCode = keyMap[controlElement.buttonCode].keyCode;
	int modifierKeyCode = keyMap[controlElement.buttonCode].modifierKeyCode;
	int repeatTime = keyMap[controlElement.buttonCode].repeatTime;
	int virtualKeyBehavior = keyMap[controlElement.buttonCode].virtualKeyBehavior;
	
	if (keyCode == -1)
		m_comboKey->SetSelection(0);
		
	if (modifierKeyCode == -1)
		m_comboModifierKey->SetSelection(0);
	
	m_comboBehavior->SetSelection(virtualKeyBehavior);
	m_txtRepeatTime->Show(virtualKeyBehavior == BEHAVIOR_REPEAT);
	m_spinRepeatTime->Show(virtualKeyBehavior == BEHAVIOR_REPEAT);	
	
	for (const VirtualKey& virtualKey : VirtualKeys)
		if (virtualKey.uinputCode == keyCode)
		{
			m_comboKey->SetStringSelection(virtualKey.keyName);
			break;
		}
		
	for (const VirtualKey& virtualModifierKey : VirtualModifierKeys)
		if (virtualModifierKey.uinputCode == modifierKeyCode)
		{
			m_comboModifierKey->SetStringSelection(virtualModifierKey.keyName);
			break;
		}
		
	m_spinRepeatTime->SetValue(repeatTime);
}

void MainFrame::editAppDialogCallback(bool newApp, wxString appName, wxString regExp)
{
	if (newApp)
	{
		TranslationSection translationSection;	
		translationSection.name = appName;
		translationSection.regex = regExp;
		m_translationSections.push_back(translationSection);
		m_comboApplication->Insert(appName, m_comboApplication->GetCount()-1);
		m_comboApplication->SetSelection(m_comboApplication->GetCount()-2);
		
		// Simulate the selection event to refresh
		wxCommandEvent commandEvent;
		OnComboApplication(commandEvent);
	}
	else
	{
		int selectedApp = m_comboApplication->GetSelection();	
		m_translationSections[selectedApp].name = appName;
		m_translationSections[selectedApp].regex = regExp;
		m_comboApplication->SetString(selectedApp, appName);
	}
}
