#ifndef EDITAPPDIALOG_H
#define EDITAPPDIALOG_H

#include "ShuttleLander_GUI.h"

class EditAppDialog
: public EditAppDialog_design
{

public:
	EditAppDialog(wxWindow* parent);
	void	preset(bool newApp, wxString appName, wxString regExp);
	
private:	
	void escapeRegexSpecials(wxString& input, bool deescape = false);
	bool regexSpecialsEscaped(const wxString& input);
	
private:
	wxWindow*	m_parent;
	bool 		m_newApp;

protected:
	// Event handlers
	void OnBtnOK(wxCommandEvent& event);
	void OnRadioIdentificationText(wxCommandEvent& event);
	void OnRadioIdentificationRegExp(wxCommandEvent& event);
};

#endif // EDITAPPDIALOG_H

