﻿///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-0-g8feb16b)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/combobox.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/sizer.h>
#include <wx/spinctrl.h>
#include <wx/panel.h>
#include "wxShuttleButtonPicker.hpp"
#include <wx/frame.h>
#include <wx/textctrl.h>
#include <wx/radiobut.h>
#include <wx/checkbox.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class MainFrame_design
///////////////////////////////////////////////////////////////////////////////
class MainFrame_design : public wxFrame
{
	private:

	protected:
		wxPanel* m_panelOptions;
		wxStaticText* m_staticText323;
		wxComboBox* m_comboApplication;
		wxButton* m_btnEdit;
		wxButton* m_btnRemove;
		wxStaticText* m_txtControlElement;
		wxStaticText* m_txtVirtualKeyBehavior;
		wxComboBox* m_comboBehavior;
		wxStaticText* m_txtKey;
		wxStaticText* m_textModifierKey;
		wxStaticText* m_txtRepeatTime;
		wxComboBox* m_comboKey;
		wxComboBox* m_comboModifierKey;
		wxSpinCtrl* m_spinRepeatTime;
		wxPanel* m_panelShuttle;
		wxShuttleButtonPicker* m_wxShuttleButtonPicker;
		wxPanel* m_panelDevice;
		wxButton* m_buttonApply;
		wxStaticText* m_txtDevice;
		wxButton* m_btnRefreshDevices;

		// Virtual event handlers, override them in your derived class
		virtual void OnComboApplication( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonAppEdit( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonAppRemove( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnComboKeyBehavor( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnComboKeyChar( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnComboKey( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnComboModifierKey( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSpinRepeatTime( wxSpinEvent& event ) { event.Skip(); }
		virtual void OnButtonApply( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonRefreshDevices( wxCommandEvent& event ) { event.Skip(); }


	public:

		MainFrame_design( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Shuttle Lander"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 675,642 ), long style = wxCAPTION|wxCLOSE_BOX|wxRESIZE_BORDER|wxTAB_TRAVERSAL );

		~MainFrame_design();

};

///////////////////////////////////////////////////////////////////////////////
/// Class EditAppDialog_design
///////////////////////////////////////////////////////////////////////////////
class EditAppDialog_design : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText12;
		wxStaticText* m_staticText10;
		wxTextCtrl* m_txtAppName;
		wxStaticText* m_staticText11;
		wxRadioButton* m_radioContainText;
		wxTextCtrl* m_txtContainsText;
		wxCheckBox* m_chbCaseSensitive;
		wxRadioButton* m_radioRegExp;
		wxTextCtrl* m_txtRegExp;
		wxButton* m_button3;

		// Virtual event handlers, override them in your derived class
		virtual void OnRadioIdentificationText( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnRadioIdentificationRegExp( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnBtnOK( wxCommandEvent& event ) { event.Skip(); }


	public:

		EditAppDialog_design( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Edit Target Application"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 671,304 ), long style = wxCAPTION|wxCLOSE_BOX );

		~EditAppDialog_design();

};

