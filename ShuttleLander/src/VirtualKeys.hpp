#ifndef VIRTUALKEYS_H
#define VIRTUALKEYS_H

#include <array>
#include "wx/string.h"
#include "linux/input-event-codes.h"

struct VirtualKey
{
	int			uinputCode;
	int			asciiCode;
	wxString	keyName;
};

const std::array<VirtualKey, 91> VirtualKeys = {{
	{KEY_ESC, 27, "ESC"},
	{KEY_1, 49, "1"},
	{KEY_2, 50, "2"},
	{KEY_3, 51, "3"},
	{KEY_4, 52, "4"},
	{KEY_5, 53, "5"},
	{KEY_6, 54, "6"},
	{KEY_7, 55, "7"},
	{KEY_8, 56, "8"},
	{KEY_9, 57, "9"},
	{KEY_0, 48, "0"},
	{KEY_A, 97, "A"},
	{KEY_B, 98, "B"},
	{KEY_C, 99, "C"},
	{KEY_D, 100, "D"},
	{KEY_E, 101, "E"},
	{KEY_F, 102, "F"},
	{KEY_G, 103, "G"},
	{KEY_H, 104, "H"},
	{KEY_I, 105, "I"},
	{KEY_J, 106, "J"},
	{KEY_K, 107, "K"},
	{KEY_L, 108, "L"},
	{KEY_M, 109, "M"},
	{KEY_N, 110, "N"},
	{KEY_O, 111, "O"},
	{KEY_P, 112, "P"},
	{KEY_Q, 113, "Q"},
	{KEY_R, 114, "R"},
	{KEY_S, 115, "S"},
	{KEY_T, 116, "T"},
	{KEY_U, 117, "U"},
	{KEY_V, 118, "V"},
	{KEY_W, 119, "W"},
	{KEY_X, 120, "X"},
	{KEY_Y, 121, "Y"},
	{KEY_Z, 122, "Z"},
	{KEY_MINUS, 45, "-"},
	{KEY_EQUAL, 61, "="},
	{KEY_BACKSPACE, 8, "BACKSPACE"},
	{KEY_TAB, 9, "TAB"},
	{KEY_LEFTSHIFT, -1, "LEFT SHIFT"},
	{KEY_RIGHTSHIFT, -1, "RIGHT SHIFT"},
	{KEY_LEFTCTRL, -1, "LEFT CTRL"},
	{KEY_RIGHTCTRL, -1, "RIGHT CTRL"},
	{KEY_LEFTALT, -1, "LEFT ALT"},
	{KEY_RIGHTALT, -1, "RIGHT ALT"},
	{KEY_ENTER, 13, "ENTER"},
	{KEY_LEFTBRACE, 123, "{"},
	{KEY_RIGHTBRACE, 125, "}"},
	{KEY_SEMICOLON, 59, ";"},
	{KEY_APOSTROPHE, 96, "'"},
	{KEY_GRAVE, -1, "GRAVE"},
	{KEY_BACKSLASH, 92, "\\"},
	{KEY_COMMA, 44, ","},
	{KEY_DOT, 46, "."},
	{KEY_SLASH, 47, "/"},
	{KEY_SPACE, 32, "SPACE"},
	{KEY_CAPSLOCK, -1, "CAPSLOCK"},
	{KEY_F1, -1, "F1"},
	{KEY_F2, -1, "F2"},
	{KEY_F3, -1, "F3"},
	{KEY_F4, -1, "F4"},
	{KEY_F5, -1, "F5"},
	{KEY_F6, -1, "F6"},
	{KEY_F7, -1, "F7"},
	{KEY_F8, -1, "F8"},
	{KEY_F9, -1, "F9"},
	{KEY_F10, -1, "F10"},
	{KEY_F11, -1, "F11"},
	{KEY_F12, -1, "F12"},
	{KEY_HOME, -1, "HOME"},
	{KEY_END, -1, "END"},
	{KEY_PAGEUP, -1, "PAGE UP"},
	{KEY_PAGEDOWN, -1, "PAGE DOWN"},
	{KEY_UP, -1, "ARROW UP"},
	{KEY_DOWN, -1, "ARROW DOWN"},
	{KEY_LEFT, -1, "ARROW LEFT"},
	{KEY_RIGHT, -1, "ARROW RIGHT"},
	{KEY_INSERT, -1, "INSERT"},
	{KEY_DELETE, -1, "DELETE"},
	{BTN_LEFT, -1, "MOUSE LEFT"},
	{BTN_MIDDLE, -1, "MOUSE MIDDLE"},
	{BTN_RIGHT, -1, "MOUSE RIGHT"},
	{REL_WHEEL + 2000, -1, "MOUSE WHEEL UP"}, // We need a little hack here as REL events are having duplicit codes with KEY events
	{REL_WHEEL + 1000, -1, "MOUSE WHEEL DOWN"}, // Also we need to seprate events for UP and DOWN
	{KEY_VOLUMEUP, -1, "VOLUME UP"},
	{KEY_VOLUMEDOWN, -1, "VOLUME DOWN"},	
	{KEY_BRIGHTNESSUP, -1, "BRIGHTNESS UP"},
	{KEY_BRIGHTNESSDOWN, -1, "BRIGHTNESS DOWN"},
	{KEY_SLEEP, -1, "SLEEP"}
}}; 

const std::array<VirtualKey, 6> VirtualModifierKeys = {{
	{KEY_LEFTSHIFT, -1, "LEFT SHIFT"},
	{KEY_RIGHTSHIFT, -1, "RIGHT SHIFT"},
	{KEY_LEFTCTRL, -1, "LEFT CTRL"},
	{KEY_RIGHTCTRL, -1, "RIGHT CTRL"},
	{KEY_LEFTALT, -1, "LEFT ALT"},
	{KEY_RIGHTALT, -1, "RIGHT ALT"}
}};

#endif // VIRTUALKEYS_H
