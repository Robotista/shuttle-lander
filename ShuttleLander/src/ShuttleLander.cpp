#include "ShuttleLander.hpp"
#include "MainFrame.hpp"

IMPLEMENT_APP(ShuttleLanderGui);

bool ShuttleLanderGui::OnInit()
{
	wxImage::AddHandler(new wxPNGHandler());

	MainFrame* frame = new MainFrame();
	frame->Show();

	return true;
}
