#include "EditAppDialog.hpp"
#include "MainFrame.hpp"
#include "wx/log.h"
#include "wx/wx.h"

EditAppDialog::EditAppDialog(wxWindow* parent)
	:EditAppDialog_design(parent)
{
	m_parent = parent;
}

void EditAppDialog::preset(bool newApp, wxString appName, wxString regExp)
{
	if (newApp)
		SetTitle(wxT("New Target Application"));
	else
		SetTitle(wxT("Edit Target Application"));	
			
	m_txtAppName->SetValue(appName);
	m_newApp = newApp;
	
	// Check if used regExp is in form suitable for "Contains Text" style identification
	bool identificationContainsText = false;
	wxString containedText = regExp;
	if ((regExp.StartsWith("(?i).*") || regExp.StartsWith(".*")) && regExp.EndsWith(".*"))
	{
		m_chbCaseSensitive->SetValue(!regExp.StartsWith("(?i).*"));
		containedText.Replace("(?i)", "");
		containedText.Replace(".*", "");
		if (regexSpecialsEscaped(containedText))
			identificationContainsText = true;
	}
	
	if (identificationContainsText)
	{
		escapeRegexSpecials(containedText, true);
		m_txtContainsText->SetValue(containedText);
		m_txtRegExp->SetValue("");
		m_radioContainText->SetValue(true);
		wxCommandEvent commandEvent;
		OnRadioIdentificationText(commandEvent);
	}
	else
	{
		m_txtContainsText->SetValue("");
		m_txtRegExp->SetValue(regExp);
		m_radioRegExp->SetValue(true);
		wxCommandEvent commandEvent;
		OnRadioIdentificationRegExp(commandEvent);
	}
}

void EditAppDialog::OnRadioIdentificationText(wxCommandEvent& WXUNUSED(event))
{
	m_txtContainsText->Enable();
	m_chbCaseSensitive->Enable();
	m_txtRegExp->Disable();
}

void EditAppDialog::OnRadioIdentificationRegExp(wxCommandEvent& WXUNUSED(event))
{
	m_txtContainsText->Disable();
	m_chbCaseSensitive->Disable();
	m_txtRegExp->Enable();
}

bool EditAppDialog::regexSpecialsEscaped(const wxString& input)
{
	std::array<wxString, 14> specials= {"\\",".","[","]","{","}","(",")","*","+","?","|","^","$"};
	
	int i = (int)input.length() - 1;
	while (i > 0)
	{
		for(wxString& special : specials)
			if (input[i] == special)
			{
				i--;
				if (input[i] != wxString("\\"))
					return false;
				break;
			}
		i--;
	}
	
	return true;
}

void EditAppDialog::escapeRegexSpecials(wxString& input, bool deescape)
{
	std::array<wxString, 14> specials= {"\\",".","[","]","{","}","(",")","*","+","?","|","^","$"};
	
	if (deescape)
	{
		int i = (int)input.length() - 1;
		while (i > 0)
		{
			for(wxString& special : specials)
				if (input[i] == special)
				{
					if (input[i - 1] == wxString("\\"))
					{
						i--;
						input.Remove(i, 1);
					}
					break;
				}
				i--;
		}		
	}
	else
	{
		for(wxString& special : specials)
			input.Replace(special, "\\" + special);
	}
}

void EditAppDialog::OnBtnOK(wxCommandEvent&)
{
	wxString appName = m_txtAppName->GetValue();
	wxString regExp = m_txtRegExp->GetValue();
	
	// If the "Contains text" identification is setted, we need to generate the regexp
	if (m_radioContainText->GetValue())
	{
		wxString text = m_txtContainsText->GetValue();
		escapeRegexSpecials(text);
		regExp = ".*" + text + ".*";
		
		if (!m_chbCaseSensitive->GetValue())
			regExp = "(?i)" + regExp;
	}
		
	if (appName.empty() || regExp.empty())
	{
		wxMessageDialog *dial = new wxMessageDialog(NULL, wxT("Application name and regular expression can not be empty"), wxT("Info"), wxOK);
		dial->ShowModal();
		return;
	}
	
	dynamic_cast<MainFrame*>(m_parent)->editAppDialogCallback(m_newApp, appName, regExp);
	Close();
}
