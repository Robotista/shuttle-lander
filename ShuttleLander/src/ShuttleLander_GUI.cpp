﻿///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-0-g8feb16b)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "ShuttleLander_GUI.h"

#include "res/icon_edit.png.h"
#include "res/icon_remove.png.h"

///////////////////////////////////////////////////////////////////////////

MainFrame_design::MainFrame_design( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 300,400 ), wxDefaultSize );
	this->SetForegroundColour( wxColour( 0, 0, 0 ) );
	this->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	wxBoxSizer* bSizer15;
	bSizer15 = new wxBoxSizer( wxVERTICAL );

	m_panelOptions = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panelOptions->SetForegroundColour( wxColour( 0, 0, 0 ) );
	m_panelOptions->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer332;
	bSizer332 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText323 = new wxStaticText( m_panelOptions, wxID_ANY, wxT("Application"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText323->Wrap( -1 );
	m_staticText323->SetFont( wxFont( 12, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Arial") ) );
	m_staticText323->SetForegroundColour( wxColour( 255, 255, 255 ) );

	bSizer332->Add( m_staticText323, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_comboApplication = new wxComboBox( m_panelOptions, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_READONLY );
	m_comboApplication->SetToolTip( wxT("Select the application the current button configuration will be assigned to") );
	m_comboApplication->SetMinSize( wxSize( 250,-1 ) );

	bSizer332->Add( m_comboApplication, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_btnEdit = new wxButton( m_panelOptions, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( -1,-1 ), wxBORDER_NONE|wxBU_EXACTFIT );

	m_btnEdit->SetBitmap( icon_edit_png_to_wx_bitmap() );
	m_btnEdit->Enable( false );
	m_btnEdit->SetToolTip( wxT("Edit current target application") );

	bSizer332->Add( m_btnEdit, 0, wxTOP|wxBOTTOM, 5 );

	m_btnRemove = new wxButton( m_panelOptions, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( -1,-1 ), wxBORDER_NONE|wxBU_EXACTFIT );

	m_btnRemove->SetBitmap( icon_remove_png_to_wx_bitmap() );
	m_btnRemove->Enable( false );
	m_btnRemove->SetToolTip( wxT("Remove current target application") );

	bSizer332->Add( m_btnRemove, 0, wxALL, 5 );


	bSizer16->Add( bSizer332, 0, wxALIGN_CENTER_HORIZONTAL|wxTOP|wxBOTTOM, 10 );

	m_txtControlElement = new wxStaticText( m_panelOptions, wxID_ANY, wxT("Select the shuttle button"), wxDefaultPosition, wxDefaultSize, 0 );
	m_txtControlElement->Wrap( -1 );
	m_txtControlElement->SetFont( wxFont( 14, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Arial") ) );
	m_txtControlElement->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_txtControlElement->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	bSizer16->Add( m_txtControlElement, 0, wxALL|wxEXPAND, 10 );

	wxBoxSizer* bSizer333;
	bSizer333 = new wxBoxSizer( wxHORIZONTAL );

	m_txtVirtualKeyBehavior = new wxStaticText( m_panelOptions, wxID_ANY, wxT("Virtual Key Behavior"), wxDefaultPosition, wxDefaultSize, 0 );
	m_txtVirtualKeyBehavior->Wrap( -1 );
	m_txtVirtualKeyBehavior->SetFont( wxFont( 11, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Arial") ) );
	m_txtVirtualKeyBehavior->SetForegroundColour( wxColour( 255, 255, 255 ) );

	bSizer333->Add( m_txtVirtualKeyBehavior, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_comboBehavior = new wxComboBox( m_panelOptions, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_READONLY );
	m_comboBehavior->SetSelection( 4 );
	m_comboBehavior->SetMinSize( wxSize( 290,-1 ) );

	bSizer333->Add( m_comboBehavior, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	bSizer16->Add( bSizer333, 0, wxEXPAND|wxLEFT, 25 );


	bSizer16->Add( 0, 0, 1, wxEXPAND|wxTOP, 15 );

	wxGridSizer* gSizer1;
	gSizer1 = new wxGridSizer( 0, 3, 0, 0 );

	m_txtKey = new wxStaticText( m_panelOptions, wxID_ANY, wxT("Key "), wxDefaultPosition, wxDefaultSize, 0 );
	m_txtKey->Wrap( -1 );
	m_txtKey->SetFont( wxFont( 10, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Arial") ) );
	m_txtKey->SetForegroundColour( wxColour( 255, 255, 255 ) );

	gSizer1->Add( m_txtKey, 0, wxTOP|wxRIGHT|wxLEFT|wxALIGN_CENTER_VERTICAL, 5 );

	m_textModifierKey = new wxStaticText( m_panelOptions, wxID_ANY, wxT("Modifier Key"), wxDefaultPosition, wxDefaultSize, 0 );
	m_textModifierKey->Wrap( -1 );
	m_textModifierKey->SetFont( wxFont( 10, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Arial") ) );
	m_textModifierKey->SetForegroundColour( wxColour( 255, 255, 255 ) );

	gSizer1->Add( m_textModifierKey, 1, wxLEFT|wxRIGHT|wxTOP|wxALIGN_CENTER_VERTICAL, 5 );

	m_txtRepeatTime = new wxStaticText( m_panelOptions, wxID_ANY, wxT("Repeat Time [ms]"), wxDefaultPosition, wxDefaultSize, 0 );
	m_txtRepeatTime->Wrap( -1 );
	m_txtRepeatTime->SetFont( wxFont( 10, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Arial") ) );
	m_txtRepeatTime->SetForegroundColour( wxColour( 255, 255, 255 ) );

	gSizer1->Add( m_txtRepeatTime, 0, wxALIGN_CENTER_VERTICAL|wxLEFT|wxRESERVE_SPACE_EVEN_IF_HIDDEN|wxRIGHT|wxTOP, 5 );

	wxBoxSizer* bSizer33;
	bSizer33 = new wxBoxSizer( wxHORIZONTAL );

	m_comboKey = new wxComboBox( m_panelOptions, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_READONLY );
	m_comboKey->SetMinSize( wxSize( 170,-1 ) );

	bSizer33->Add( m_comboKey, 1, wxALL, 5 );


	gSizer1->Add( bSizer33, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer331;
	bSizer331 = new wxBoxSizer( wxHORIZONTAL );

	m_comboModifierKey = new wxComboBox( m_panelOptions, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_READONLY );
	m_comboModifierKey->SetMinSize( wxSize( 140,-1 ) );

	bSizer331->Add( m_comboModifierKey, 1, wxALL, 5 );


	gSizer1->Add( bSizer331, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer3311;
	bSizer3311 = new wxBoxSizer( wxHORIZONTAL );

	m_spinRepeatTime = new wxSpinCtrl( m_panelOptions, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10000, 1000 );
	m_spinRepeatTime->SetMinSize( wxSize( 150,-1 ) );

	bSizer3311->Add( m_spinRepeatTime, 0, wxALL|wxRESERVE_SPACE_EVEN_IF_HIDDEN, 5 );


	gSizer1->Add( bSizer3311, 1, wxEXPAND, 5 );


	bSizer16->Add( gSizer1, 0, wxEXPAND|wxLEFT, 50 );

	wxBoxSizer* bSizer31;
	bSizer31 = new wxBoxSizer( wxHORIZONTAL );


	bSizer16->Add( bSizer31, 2, wxEXPAND|wxLEFT, 25 );


	m_panelOptions->SetSizer( bSizer16 );
	m_panelOptions->Layout();
	bSizer16->Fit( m_panelOptions );
	bSizer15->Add( m_panelOptions, 0, wxEXPAND|wxRESERVE_SPACE_EVEN_IF_HIDDEN, 5 );

	m_panelShuttle = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer27;
	bSizer27 = new wxBoxSizer( wxVERTICAL );

	m_wxShuttleButtonPicker = new wxShuttleButtonPicker(m_panelShuttle);
	m_wxShuttleButtonPicker->SetForegroundColour( wxColour( 0, 0, 0 ) );
	m_wxShuttleButtonPicker->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	bSizer27->Add( m_wxShuttleButtonPicker, 1, wxEXPAND, 5 );


	m_panelShuttle->SetSizer( bSizer27 );
	m_panelShuttle->Layout();
	bSizer27->Fit( m_panelShuttle );
	bSizer15->Add( m_panelShuttle, 1, wxEXPAND, 5 );

	m_panelDevice = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panelDevice->SetForegroundColour( wxColour( 0, 0, 0 ) );
	m_panelDevice->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	wxBoxSizer* bSizer32;
	bSizer32 = new wxBoxSizer( wxVERTICAL );

	m_buttonApply = new wxButton( m_panelDevice, wxID_ANY, wxT("Apply"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer32->Add( m_buttonApply, 0, wxALIGN_RIGHT|wxBOTTOM|wxRIGHT, 15 );

	m_txtDevice = new wxStaticText( m_panelDevice, wxID_ANY, wxT("No device detected"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER_HORIZONTAL );
	m_txtDevice->Wrap( -1 );
	m_txtDevice->SetFont( wxFont( 12, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Arial Black") ) );
	m_txtDevice->SetForegroundColour( wxColour( 255, 255, 255 ) );
	m_txtDevice->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	bSizer32->Add( m_txtDevice, 0, wxALL|wxEXPAND, 5 );

	m_btnRefreshDevices = new wxButton( m_panelDevice, wxID_ANY, wxT("Refresh"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer32->Add( m_btnRefreshDevices, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 10 );


	m_panelDevice->SetSizer( bSizer32 );
	m_panelDevice->Layout();
	bSizer32->Fit( m_panelDevice );
	bSizer15->Add( m_panelDevice, 0, wxEXPAND, 5 );


	this->SetSizer( bSizer15 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_comboApplication->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( MainFrame_design::OnComboApplication ), NULL, this );
	m_btnEdit->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame_design::OnButtonAppEdit ), NULL, this );
	m_btnRemove->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame_design::OnButtonAppRemove ), NULL, this );
	m_comboBehavior->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( MainFrame_design::OnComboKeyBehavor ), NULL, this );
	m_comboKey->Connect( wxEVT_CHAR, wxKeyEventHandler( MainFrame_design::OnComboKeyChar ), NULL, this );
	m_comboKey->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( MainFrame_design::OnComboKey ), NULL, this );
	m_comboModifierKey->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( MainFrame_design::OnComboModifierKey ), NULL, this );
	m_spinRepeatTime->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( MainFrame_design::OnSpinRepeatTime ), NULL, this );
	m_buttonApply->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame_design::OnButtonApply ), NULL, this );
	m_btnRefreshDevices->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame_design::OnButtonRefreshDevices ), NULL, this );
}

MainFrame_design::~MainFrame_design()
{
	// Disconnect Events
	m_comboApplication->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( MainFrame_design::OnComboApplication ), NULL, this );
	m_btnEdit->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame_design::OnButtonAppEdit ), NULL, this );
	m_btnRemove->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame_design::OnButtonAppRemove ), NULL, this );
	m_comboBehavior->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( MainFrame_design::OnComboKeyBehavor ), NULL, this );
	m_comboKey->Disconnect( wxEVT_CHAR, wxKeyEventHandler( MainFrame_design::OnComboKeyChar ), NULL, this );
	m_comboKey->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( MainFrame_design::OnComboKey ), NULL, this );
	m_comboModifierKey->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( MainFrame_design::OnComboModifierKey ), NULL, this );
	m_spinRepeatTime->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( MainFrame_design::OnSpinRepeatTime ), NULL, this );
	m_buttonApply->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame_design::OnButtonApply ), NULL, this );
	m_btnRefreshDevices->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame_design::OnButtonRefreshDevices ), NULL, this );

}

EditAppDialog_design::EditAppDialog_design( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer( wxVERTICAL );

	m_staticText12 = new wxStaticText( this, wxID_ANY, wxT("Applications are identified by the text in their title bar. If the targeted app window is in focus the respective button configuration will be employed."), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText12->Wrap( 550 );
	bSizer11->Add( m_staticText12, 1, wxALL|wxEXPAND, 10 );


	bSizer11->Add( 0, 20, 0, wxEXPAND, 5 );

	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 2, 2, 0, 0 );
	fgSizer1->AddGrowableCol( 1 );
	fgSizer1->SetFlexibleDirection( wxBOTH );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_staticText10 = new wxStaticText( this, wxID_ANY, wxT("Application name"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10->Wrap( -1 );
	fgSizer1->Add( m_staticText10, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_txtAppName = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_txtAppName->SetToolTip( wxT("Something to describe this target application") );

	fgSizer1->Add( m_txtAppName, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_staticText11 = new wxStaticText( this, wxID_ANY, wxT("App title identification"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText11->Wrap( -1 );
	fgSizer1->Add( m_staticText11, 0, wxALL, 5 );

	wxFlexGridSizer* fgSizer2;
	fgSizer2 = new wxFlexGridSizer( 2, 3, 0, 0 );
	fgSizer2->AddGrowableCol( 1 );
	fgSizer2->AddGrowableRow( 0 );
	fgSizer2->SetFlexibleDirection( wxBOTH );
	fgSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_radioContainText = new wxRadioButton( this, wxID_ANY, wxT("Contains text"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer2->Add( m_radioContainText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_txtContainsText = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_txtContainsText->SetToolTip( wxT("Text contained in the title bar of your target application") );

	fgSizer2->Add( m_txtContainsText, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_chbCaseSensitive = new wxCheckBox( this, wxID_ANY, wxT("Case sensitive"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer2->Add( m_chbCaseSensitive, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_radioRegExp = new wxRadioButton( this, wxID_ANY, wxT("Custom Reg. Exp."), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer2->Add( m_radioRegExp, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_txtRegExp = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_txtRegExp->Enable( false );
	m_txtRegExp->SetToolTip( wxT("Matching with regular expression (POSIX) - if you want to go full nerdy") );

	fgSizer2->Add( m_txtRegExp, 0, wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );


	fgSizer1->Add( fgSizer2, 1, wxEXPAND, 5 );


	bSizer11->Add( fgSizer1, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );


	bSizer11->Add( 0, 10, 1, wxEXPAND, 5 );

	m_button3 = new wxButton( this, wxID_ANY, wxT("Ok"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, wxT("uuu") );
	bSizer11->Add( m_button3, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 10 );


	this->SetSizer( bSizer11 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_radioContainText->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( EditAppDialog_design::OnRadioIdentificationText ), NULL, this );
	m_radioRegExp->Connect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( EditAppDialog_design::OnRadioIdentificationRegExp ), NULL, this );
	m_button3->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( EditAppDialog_design::OnBtnOK ), NULL, this );
}

EditAppDialog_design::~EditAppDialog_design()
{
	// Disconnect Events
	m_radioContainText->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( EditAppDialog_design::OnRadioIdentificationText ), NULL, this );
	m_radioRegExp->Disconnect( wxEVT_COMMAND_RADIOBUTTON_SELECTED, wxCommandEventHandler( EditAppDialog_design::OnRadioIdentificationRegExp ), NULL, this );
	m_button3->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( EditAppDialog_design::OnBtnOK ), NULL, this );

}
